/* Antigenic phenotype present in individual Viruses and within Hosts as immune history */
/* Should be able to calculate distance and cross-immunity between two phenotypes */
/* Moving up to multiple dimensions is non-trivial and requires thought on the implementation */
/* Multiple Viruses can reference a single Phenotype object */


public class GeometricPhenotype implements Phenotype {
	
	// parameters specific to GeometricPhenotype
	@Setting static double smithConversion = 0.07;				// multiplier to distance to give cross-immunity	
	@Setting static double initialTraitA = -10;
	@Setting static double initialTraitB = 0.0;	
	@Setting static double meanStep = 0.55; 
	@Setting static boolean mut2D = true;						// whether to mutate in a full 360 degree arc
	

	// fields
	private double traitA;
	private double traitB;	
	
	// constructor
	public GeometricPhenotype() {
	
	}
	public GeometricPhenotype(double tA, double tB) {
		traitA = tA;
		traitB = tB;
	}
		
	public double getTraitA() {
		return traitA;
	}
	public double getTraitB() {
		return traitB;
	}	
	
	public void setTraitA(double tA) {
		traitA = tA;
	}
	public void setTraitB(double tB) {
		traitB = tB;
	}		
		
	// raw antigenic distance between two phenotypes
	public double distance(Phenotype p) {
		GeometricPhenotype p2d = (GeometricPhenotype) p;
		double distA = (getTraitA() - p2d.getTraitA());
		double distB = (getTraitB() - p2d.getTraitB());	
		double dist = (distA * distA) + (distB * distB);
		dist = Math.sqrt(dist);
		return dist;
	}

	// cross immunity between a virus phenotype and a host's immune history
	// here encoded more directly as risk of infection, which ranges from 0 to 1
	public double riskOfInfection(ImmuneSystem immuneSystem) {
	
		ImmuneSystemPhenotypeHistoryBased history = (ImmuneSystemPhenotypeHistoryBased) immuneSystem;
		
		// find closest phenotype in history
		double closestDistance = 100.0;
		if (history.length() > 0) {
			for (int i = 0; i < history.length(); i++) {
				double thisDistance = distance(history.get(i));
				if (thisDistance < closestDistance) {
					closestDistance = thisDistance;
				}
				if (thisDistance < 0.01) {
					break;
				}
			}
		} 
		
		double risk = closestDistance * smithConversion;
		risk = Math.max(0.01, risk);
		risk = Math.min(1.0, risk);
				
		return risk;
		
	}
	
	// returns a mutated copy, original Phenotype is unharmed
	public Phenotype mutate() {
		
		// direction of mutation
		double theta = 0;
		if (mut2D) {
			theta = Random.nextDouble(0,2*Math.PI);
		} else {
			if (Random.nextBoolean(0.5)) { theta = 0; }
			else { theta = Math.PI; }
		}
		
		// size of mutation
		double r = Random.nextExponential(meanStep);
			
		double mutA = getTraitA() + r * Math.cos(theta);
		double mutB = getTraitB() + r * Math.sin(theta);
		Phenotype mutP = new GeometricPhenotype(mutA,mutB);
		return mutP;
				
	}
	
	public String toString(String seperator) {
		String fullString = String.format("%.4f%s%.4f", traitA,seperator, traitB);
		return fullString;
	}
	@Override
	public void add(ImmuneSystem immuneSystem) {
		immuneSystem.add(this);
		
	}
	
	@Override
	public Number getPhenotypePart(int n) {
		switch (n) {
		case 0: return getTraitA();
		case 1: return getTraitB();
		default: {System.err.println("only two parts (0 and 1) to phenotype..."); return 0;}
		}
		
	}
	
	@Override
	public int numParts() {
		return 2;
	}
	@Override
	public long getIndex() {
		// TODO Auto-generated method stub		
		// this isn't implemented...
		return 0;
	}
	@Override
	public long getNumPossibleStrains() {	
		return 0;
	}
	


}