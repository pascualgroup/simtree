/* Antigenic phenotype present in individual Viruses and within Hosts as immune history */
/* Should be able to calculate distance and cross-immunity between two phenotypes */
/* Moving up to multiple dimensions is non-trivial and requires thought on the implementation */
/* Multiple Viruses can reference a single Phenotype object */


import java.util.*;

public class PhenotypeLimitedDiversitySunetra implements Phenotype {

	// DANIEL 
	// parameters specific to PhenotypeLimitedDiversitySunetra
	@Setting static double gamma = 0.5;					
	@Setting static double delta = 0.0;						
	@Setting static int nLoci = 5;	

	// Quick Reference for Risk Of Infection
	static double riskNoneEncountered = 1.0;
	static double riskOneAlleleEncountered = (1.0-gamma); 
	static double riskMoreThanOneAlleleEncountered = (1.0-gamma)*(1-delta);
	static double riskExactlySameStrainEncountered = 0;
	
	// DANIEL 
	// parameters specific to PhenotypeLimitedDiversitySunetra
	static int nEpitopes = 0;  // in init()
	static int numPossibleStrains = 1; // in init()      	
	static int[] multiplyForIndex = {1,1,1,1,1};//,1,1,1}; //{1,2,6,24,120}; // in init()
	static int[] sumForIndex = {0,0,0,0,0};//,0,0,0}; // {0,2,5,9,14,20}; // in init()
	@Setting static final int[] nStatesPerLoci =   {2,2,2,2,2};//{2,3,4,5,6};
	static final BitSet initialLimitedDiversityPhenotype = new BitSet(Parameters.defaultNumEpitopes);	// in init()
	public static enum PhenotypeMutationOptionType {Flipping, SingleTrait, NoMutation};
	@Setting public static PhenotypeMutationOptionType PhenotypeMutationOption = PhenotypeMutationOptionType.SingleTrait; 

	// fields		
	private BitSet traits = new BitSet(Parameters.defaultNumEpitopes); 
	private int index;
	public static boolean initialized = false;

	// phenotype output
	static final boolean outputBinary = false;

	public static int getNumEpitopes() {
		return nEpitopes;
	}
	
	public static void init() {		

		numPossibleStrains=1;
		for (int i=0;i<nStatesPerLoci.length;i++) {
			numPossibleStrains*=nStatesPerLoci[i];
		}	

		nEpitopes=0;
		for (int i=0;i<nStatesPerLoci.length;i++) {
			nEpitopes+=nStatesPerLoci[i];
		}	

		sumForIndex[0]=0;
		for (int i=1;i<nLoci;i++) {
			sumForIndex[i]=sumForIndex[i-1]+nStatesPerLoci[i-1];
		}

		multiplyForIndex[0]=1;
		for (int i=1;i<nLoci;i++) {
			multiplyForIndex[i]=multiplyForIndex[i-1]*nStatesPerLoci[i-1];
		}

		for (int i=0;i<nLoci;i++) {
			initialLimitedDiversityPhenotype.set(sumForIndex[i]);
		}

		if (Parameters.param_index>=0) {

			double[] gamma_range = new double[Parameters.gridpoints];
			for (int i=0; i<Parameters.gridpoints; i++) {
				gamma_range[i]=0+((double) i)/((double)Parameters.gridpoints-1.0);
			}

			gamma = gamma_range[Parameters.param_index%gamma_range.length];
			delta=gamma; // either this or fix delta at an arbitray value ??? 

		}
		if (Parameters.random_key_params) {
			gamma = Random.nextDouble(0, 1);
		}
		
		// Quick Reference for Risk Of Infection
		riskNoneEncountered = 1.0;
		riskOneAlleleEncountered = (1.0-gamma);
		riskMoreThanOneAlleleEncountered = (1.0-gamma)*(1-delta);
		riskExactlySameStrainEncountered = 0;

	}

	// constructors
	public PhenotypeLimitedDiversitySunetra() {

		if (!initialized) {
			init();
		}
		traits = new BitSet();

		for (int i=0;i<nLoci;i++) {
			traits.set(sumForIndex[i]);
		}

		index=(-1);

	}

	public PhenotypeLimitedDiversitySunetra(BitSet _traits) {			
		traits=(BitSet) _traits.clone();	
		index = (-1);
	}




	//public BitSet[] getTraits() {
	//	return traits;
	//}

	// cross immunity between a virus phenotype and a host's immune history
	// (1-gamma) if any of the alleles has been encountered before
	// (1-gamma)x(1-delta) if all the alleles have been encountered before
	@Override
	public double riskOfInfection( ImmuneSystem immuneSystem) {
		
		if (((ImmuneSystemEpitopeNPhenotypeHistoryBased) immuneSystem).encounteredPhenotype((int)this.getIndex()))
			return riskExactlySameStrainEncountered;

		BitSet hasEncounteredBefore = (BitSet) ((ImmuneSystemEpitopeNPhenotypeHistoryBased) immuneSystem).getHistory().clone();		
		hasEncounteredBefore.and(traits);			
		int numAllelesEncounteredBefore = hasEncounteredBefore.cardinality();
		
		double risk;
		
		
		switch (numAllelesEncounteredBefore) {
			case 0 : risk = riskNoneEncountered; break;
			case 1 : risk = riskOneAlleleEncountered; break;
			default : risk = riskMoreThanOneAlleleEncountered; break;
		}
		
				
		return risk;

	}


	// returns a random set of phenotype traits
	public static BitSet randomTraits() {
		BitSet returnValue = new BitSet();

		for (int i=0; i<nLoci;i++) {			
			returnValue.set(sumForIndex[i]+Random.nextInt(0, nStatesPerLoci[i]-1)); 		
		}

		return returnValue;
	}

	@Override
	public Phenotype mutate() {

		switch (PhenotypeMutationOption) {
		case Flipping:
			return new PhenotypeLimitedDiversitySunetra(randomTraits()); // returns a mutated version - this is basically a random version for this model

		case SingleTrait:
			// randoms one of the traits....

			BitSet newTraits = (BitSet) traits.clone();

			int randLoci = Random.nextInt(0, nLoci-1);
			newTraits.clear(sumForIndex[randLoci],sumForIndex[randLoci]+nStatesPerLoci[randLoci]);	
			newTraits.set(sumForIndex[randLoci]+Random.nextInt(0, nStatesPerLoci[randLoci]-1));
			if (!newTraits.equals(traits)) {
				return new PhenotypeLimitedDiversitySunetra(newTraits);
			}
			else {
				return this;
			}

		case NoMutation: default: 
			return new PhenotypeLimitedDiversitySunetra(traits);		
		}



	}

	@Override
	public String toString() {
		return toString(",");
	}

	// returns allele version at each loci "3,2,....,allele_version_at_loci_n"
	@Override
	public String toString(String seperator) {

		String returnValue = "";

		if (outputBinary) {
			for (int i=0; i<nEpitopes; i++) {
				if (traits.get(i)) {
					returnValue = returnValue + "0";
				}
				else {
					returnValue = returnValue + "1";
				}
				if (i!=(nEpitopes-1)) {
					returnValue = returnValue +seperator;
				}			
			}			
		}
		else {
			for (int i=0; i<nLoci; i++) {
				for (int j=0; j<nStatesPerLoci[i]; j++) {
					if (traits.get(sumForIndex[i]+j)) {
						returnValue = returnValue + j; 														
						if (i!=(nLoci-1)) {
							returnValue = returnValue + seperator;
						}
					}
				}
			}
		}

		return returnValue;
	}

	public boolean increaseTraits() {
		
		index = (-1);

		int[] intTraits = new int[nLoci];

		BitSet currentEpitope = new BitSet();
		BitSet newTraits = new BitSet();

		for (int i=0; i<nLoci; i++) {
			currentEpitope = traits.get(sumForIndex[i], sumForIndex[i]+nStatesPerLoci[i]);				
			intTraits[i]=currentEpitope.nextSetBit(0);
		}

		intTraits[0]=intTraits[0]+1;
		for (int i=0;i<(intTraits.length-1);i++) {
			if (intTraits[i]>=PhenotypeLimitedDiversitySunetra.nStatesPerLoci[i]) {
				intTraits[i]=0;
				intTraits[i+1]=intTraits[i+1]+1;
			}
		}
		if (intTraits[PhenotypeLimitedDiversitySunetra.nLoci-1]<PhenotypeLimitedDiversitySunetra.nStatesPerLoci[PhenotypeLimitedDiversitySunetra.nLoci-1]) {

			for (int i=0; i<nLoci; i++) {
				newTraits.set(sumForIndex[i]+intTraits[i]);
			}					
			traits = newTraits;
			return true;
		} 
		else {		
			traits = initialLimitedDiversityPhenotype;
			return false;
		}

	}


	public long getIndex() {

		if (index==(-1)) {

			BitSet currentEpitope = new BitSet();

			int returnValue = 0;

			for (short i=0; i<nLoci; i++) {
				currentEpitope = traits.get(sumForIndex[i], sumForIndex[i]+nStatesPerLoci[i]);				
				returnValue+=currentEpitope.nextSetBit(0)*multiplyForIndex[i];
			}

			index = returnValue;

			return returnValue;
		}
		else {
			return index;
		}		
	}

	@Override
	public void add(ImmuneSystem immuneSystem) {
		((ImmuneSystemEpitopeNPhenotypeHistoryBased) immuneSystem).add(traits, (int)getIndex());

	}

	public BitSet getTraits() {
		// TODO Auto-generated method stub
		return traits;
	}
	
	@Override
	public Number getPhenotypePart(int n) {
		if ((n >= nLoci) || (n<0)) {
			System.err.println("only parts 0.."+nLoci+" in phenotype..."); return 0; 
		}
		else {
			for (int j=0; j<nStatesPerLoci[n]; j++) {
				if (traits.get(sumForIndex[n]+j)) {
					return j; 																			
				}
			}
		}
		System.err.println("error: part "+ n + " of phenotype not found!\n");
		return 0;
	}
	
	@Override
	public int numParts() {
		return nLoci;
	}

	@Override
	public long getNumPossibleStrains() {		
		return numPossibleStrains;
	}



}
