import java.io.IOException;
import java.text.DecimalFormat;

public class Fx implements Comparable<Fx> {

	// simulation input
	double[] xs;
	String epitopeConfig="";
	String popSize = "";

	// input related constants
	final static int betaI=0;
	final static int nuI=1;
	final static int slopeI=2;
	final static int muPI=3;
	//final static int iPrRI=4;
	final static int burninI=4;
	final static int ampTempI=5;
	final static int ampTropI=6;
	final static int epitopeStartI=7;


	// parameter range
	double[] xmin;
	double[] xmax;
	private double popSizeMultiplier = 1;
	static final int[] finalPopSize = {20000000,20000000,10000000};
	static final int minEpitopesPerLoci = 2;
	static final int maxEpitopesPerLoci = 12;

	// simulation results
	private double meanDiv = 999;
	private double maxDiv =999;
	private double minDiv =999;
	private double numStrains =999;
	private double maxGap =999;
	private double inc = 0;
	private double minInfectedP= 0;
	double result;

	// constants
	final static public int oIsBETTER = 1;
	final static public int oIsEQUAL = 0;
	final static public int oIsWORSE = -1;
	
	// additional constraints
	final static public int maxEpitopes = 64;
	final static public int maxStrains=8000;



	DecimalFormat df = new DecimalFormat("#.##");

	public Fx(double popSizeMultiplier_, double[] xs_,double[] xmin_, double[] xmax_) throws IOException {
		xs=xs_.clone();
		xmin=xmin_.clone();
		xmax=xmax_.clone();		
		popSizeMultiplier=popSizeMultiplier_;
		calcFx();

	}


	public Fx(double popSizeMultiplier_,double[] xs_,double[] xmin_, double[] xmax_, double result_) {
		xs=xs_.clone();
		xmin=xmin_.clone();
		xmax=xmax_.clone();
		
		epitopeConfig="";
		for (int i=epitopeStartI;i<xs.length-1;i++) {
			epitopeConfig=epitopeConfig+(int)xs[i]+",";
		}
		epitopeConfig=epitopeConfig+(int)xs[xs.length-1];
				
		popSizeMultiplier=popSizeMultiplier_;
		result=result_;


	}


	public Fx duplicate() {
		return new Fx(popSizeMultiplier,xs,xmin,xmax,result);
	}

//		private void calcFx() {		
//		
//			epitopeConfig="";
//			for (int i=5;i<xs.length-1;i++) {
//				epitopeConfig=epitopeConfig+(int)xs[i]+",";
//			}
//			epitopeConfig=epitopeConfig+(int)xs[xs.length-1];	
//			result =(xs[0]*xs[1]/xs[2]+xs[0]+xs[2]*xs[1]+xs[4]*xs[2]+xs[5]/xs[4]+xs[5]/(Random.nextDouble(0.91,1)*xs[3]));
//			for (int i=0;i<xs.length;i++) {
//				result=result+xs[i];
//			}
//			result=result-xs[xs.length-1]*10;		
//			System.err.println(result);		
//		}

	public void printFx() {
		System.out.println("f(beta="+df.format(xs[betaI])+" nu="+df.format(xs[nuI])+" slope="+df.format(xs[slopeI])+" muP="+df.format(xs[muPI])+" burnin="+df.format(xs[burninI])+"y initialNs="+popSize+" ampTrp="+df.format(xs[ampTropI])+" ampTemp="+df.format(xs[ampTempI])+")="+df.format(result)+"\n\tepitope_config="+epitopeConfig+"\n\tmeanDiv="+df.format(meanDiv)+" maxDiv="+df.format(maxDiv)+" minDiv="+df.format(minDiv)+" numStrains="+numStrains+" maxGap="+df.format(maxGap)+" inc="+df.format(inc)+" minIP(*1E5)="+df.format(minInfectedP*100000));
	}

	public String stringFx() {
		return "f(beta="+df.format(xs[betaI])+" nu="+df.format(xs[nuI])+" slope="+df.format(xs[slopeI])+" muP="+df.format(xs[muPI])+" burnin="+df.format(xs[burninI])+"y initialNs="+popSize+" ampTrp="+df.format(xs[ampTropI])+" ampTemp="+df.format(xs[ampTempI])+")="+df.format(result)+"\n\tepitope_config="+epitopeConfig+"\n\tmeanDiv="+df.format(meanDiv)+" maxDiv="+df.format(maxDiv)+" minDiv="+df.format(minDiv)+" numStrains="+numStrains+" maxGap="+df.format(maxGap)+" inc="+df.format(inc)+" minIP(*1E5)="+df.format(minInfectedP*100000)+"\n";
	}

	private void calcFx() throws IOException {
		
		// simulation results
		double meanDiv1 = 999;
		double maxDiv1 =999;
		double minDiv1 =999;
		double numStrains1 =999;
		double maxGap1 =999;
		double inc1 = 0;
		double minInfectedP1= 0;
		// simulation results
		double meanDiv2 = 999;
		double maxDiv2 =999;
		double minDiv2 =999;
		double numStrains2 =999;
		double maxGap2 =999;
		double inc2 = 0;
		double minInfectedP2= 0;

		Simulation sim = new Simulation();	

		epitopeConfig="";
		for (int i=epitopeStartI;i<xs.length-1;i++) {
			epitopeConfig=epitopeConfig+(int)xs[i]+",";
		}
		epitopeConfig=epitopeConfig+(int)xs[xs.length-1];
		
		popSize="";
		for (int i=0;i<finalPopSize.length-1;i++) {
			popSize+=(((int)Math.round(finalPopSize[i]*popSizeMultiplier))+",");
		}
		popSize+=(((int)Math.round(finalPopSize[finalPopSize.length-1]*popSizeMultiplier)));
		
		int burnin = (int) Math.round(xs[burninI]*365);
		int endDay = burnin + 365*40;
		
		Parameters.applyArgsAndInit(new String[]{"beta="+xs[betaI],"nu="+xs[nuI],"slope="+xs[slopeI]," muPhenotype="+Math.pow(10,xs[muPI])," burnin="+burnin,"nLoci="+(xs.length-epitopeStartI),"endDay="+endDay,"nStatesPerLoci="+epitopeConfig,"initialNs="+popSize,"demeAmplitudes1="+xs[ampTempI]+",0,"+xs[ampTempI],"demeAmplitudes2="+"0,"+xs[ampTropI]+",0","silent=true"});
		Parameters.printParams();

		sim.reset();
		sim.run();

		meanDiv1 = sim.getMeanDiversity();
		maxDiv1= sim.getMaxDiversity();
		minDiv1 = sim.getMinDiversity();
		numStrains1 = sim.getNumSignificantStrains();
		maxGap1 = sim.getMaxGapSize();
		inc1 = sim.getYearlyIncidancePercent();
		minInfectedP1 = sim.getMinInfectedP();
		
		Parameters.applyArgsAndInit(new String[]{"beta="+xs[betaI],"nu="+xs[nuI],"slope="+xs[slopeI]," muPhenotype="+Math.pow(10,xs[muPI])," burnin="+burnin,"nLoci="+(xs.length-epitopeStartI),"endDay="+endDay,"nStatesPerLoci="+epitopeConfig,"initialNs="+popSize,"demeAmplitudes1="+xs[ampTempI]+",0,"+xs[ampTempI],"demeAmplitudes2="+"0,"+xs[ampTropI]+",0","silent=true"});

		sim.reset();
		sim.run();

		meanDiv2 = sim.getMeanDiversity();
		maxDiv2= sim.getMaxDiversity();
		minDiv2 = sim.getMinDiversity();
		numStrains2 = sim.getNumSignificantStrains();
		maxGap2 = sim.getMaxGapSize();
		inc2 = sim.getYearlyIncidancePercent();
		minInfectedP2 = sim.getMinInfectedP();
			
		meanDiv=Math.max(meanDiv1,meanDiv2);
		maxDiv= Math.max(maxDiv1,maxDiv2);
		minDiv= Math.min(minDiv1,minDiv2)/2;
		numStrains=(numStrains1+numStrains2)/2;
		maxGap=Math.max(maxGap1,maxGap2);
		inc=(inc1+inc2)/2;
		minInfectedP=(minInfectedP1+minInfectedP2)/2;
		
		result=goalFunction(meanDiv,maxDiv,minDiv,numStrains,maxGap,inc,minInfectedP);
	}
	
	

	private double goalFunction(double meanDiv, double maxDiv,
			double minDiv, double numStrains, double maxGap, double inc,
			double minInfectedP) {
		double result = 0;
		
		result = result+((6.5-maxDiv)/6.5)*((6.5-maxDiv)/6.5);
		
		if (maxDiv>9.5) { 
			result+=10;
		}
			
		if (numStrains<=19)
			result = result+((15-numStrains)/15)*((15-numStrains)/15);
		if (numStrains>19)
			result = result+((34-numStrains)/34)*((34-numStrains)/34);

		result = result+((2.5-maxGap)/2.5)*((2.5-maxGap)/2.5);
		
		if (maxGap>4) {
			result+=10; 
		}
		
		result = result+((8-inc)/20)*((8-inc)/20);
		
		if (inc<6.5 || inc>14) {
			result+=10;
		}
		
		if (minInfectedP<0.00002) {
			result+=10;
		}
		
		if (minInfectedP<0.0002) { 	
			result=result+((minInfectedP-0.0002)/0.0024)*((minInfectedP-0.0002)/0.0024);
		}
		return result;
	}


	public double fx() {			
		return result;		
	}

	public double[] getXs() {
		return xs;
	}

	@Override
	public int compareTo(Fx o) {
		if (result<o.fx()) return oIsWORSE;
		else if (result>o.fx()) return oIsBETTER;
		else return oIsEQUAL;

	}

	public Fx neighbour(double populationSizeMultiplier) throws IOException {
		double[] newxs = xs.clone();

		final double mutateEpitopes = 0;		
		final double mutateParameters = 1;

		if (Random.nextDouble(0,1)<mutateEpitopes) {
			final double keepNumLoci = 0.8;
			final double addLoci = 0.5;			

			if (Random.nextDouble(0,1)<keepNumLoci) {
				int numMutationSites = (int) Math.round(Random.nextExponential(2));
				for (int i=0;i<numMutationSites;i++) {
					int site_index = Random.nextInt(epitopeStartI,xs.length-1);					
					newxs[site_index]=mutateIndividualEpitope(site_index);
				}
			} else {
				if (Random.nextDouble(0,1)<addLoci) {
					newxs = new double[xs.length+1];
					for (int i=0;i<xs.length;i++) {
						newxs[i]=xs[i];
					}
					newxs[xs.length]=Random.nextInt(minEpitopesPerLoci, maxEpitopesPerLoci);
				} else {	// remove loci
					if (xs.length>(epitopeStartI+1)) {			
						newxs = new double[xs.length-1];
						int removalPoint=Random.nextInt(epitopeStartI, xs.length-1);
						for (int i=0;i<removalPoint;i++) {
							newxs[i]=xs[i];
						}					
						for (int i=removalPoint;i<xs.length;i++) {
							newxs[i-1]=xs[i];
						}
					}
				}
			}
		}

		if (calcNumStrains(newxs)>maxStrains || calcNumEpitopes(newxs)>maxEpitopes) {
			newxs=xs.clone();
		}

		if (Random.nextDouble(0,1)<mutateParameters) {
			int numMutationSites = Random.nextInt(1, epitopeStartI*3/2);
			for (int i=0;i<numMutationSites;i++) {
				int site_index = Random.nextInt(0,epitopeStartI-1);
				newxs[site_index]=mutateIndividualParameter(site_index);
			}

		}
		
		return new Fx(populationSizeMultiplier,newxs,xmin,xmax);
	}
	
	public void rerun(double popSizeMultiplier_) throws IOException {
		popSizeMultiplier=popSizeMultiplier_;
		calcFx();
	}

	private double calcNumEpitopes(double[] newxs) {
		double returnValue=0;
		for (int i=epitopeStartI;i<newxs.length;i++) {
			returnValue+=newxs[i];
		}
		return returnValue;
		
	}


	private double calcNumStrains(double[] newxs) {
		double returnValue=1;
		for (int i=epitopeStartI;i<newxs.length;i++) {
			returnValue*=newxs[i];
		}
		return returnValue;
	}


	private double mutateIndividualEpitope(int site_index) {			
		return Random.nextInt(minEpitopesPerLoci,maxEpitopesPerLoci);
	}

	private double mutateIndividualParameter(int site_index) {		
		double returnValue = 0;
		switch (site_index) {
		case betaI: case nuI : case slopeI : case muPI : case ampTempI : case ampTropI:  
			double step = Math.abs(xmax[site_index]-xmin[site_index])/2;
			returnValue=Random.nextDouble(xs[site_index]-step, xs[site_index]+step);
			if (returnValue>xmax[site_index])
				returnValue=xmax[site_index];
			if (returnValue<xmin[site_index])
				returnValue=xmin[site_index];
			return returnValue;		
		case burninI :
			returnValue=burninI + Random.nextInt(-35, 30);
			if (returnValue>xmax[site_index])
				returnValue=xmax[site_index];
			if (returnValue<xmin[site_index])
				returnValue=xmin[site_index];
			return returnValue;
			
		default:
			System.err.println("mutating wrong site:"+site_index);			
		}
		System.err.println("error mutating!"+site_index);
		return 0;
	}


	public Fx recombine(double populationSizeMultiplier, Fx fx2) throws IOException {
		double[] newxs = xs.clone();;

		final double recombineEpitopes = 0;		
		final double recombineParameters = 1;
		final double recombineMean = 0.5;

		if (Random.nextDouble(0,1)<recombineEpitopes) {
			int numRecombinationSites = (int) Math.round(Random.nextExponential(xs.length/2));
			for (int i=0;i<numRecombinationSites;i++) {
				int site_index = Random.nextInt(epitopeStartI,xs.length-1);
				if (site_index<fx2.xs.length) { 
					newxs[site_index]=fx2.xs[site_index];
				}
			}		
		}

		if (Random.nextDouble(0,1)<recombineParameters) {
			int numRecombinationSites = (int) Math.round(Random.nextExponential(3));
			for (int i=0;i<numRecombinationSites;i++) {
				int site_index = Random.nextInt(0,epitopeStartI-1);
				newxs[site_index]=fx2.xs[site_index];
			}
		}

		if (Random.nextDouble(0,1)<recombineMean) {			
			if (Random.nextDouble(0,1)<recombineParameters) {
				for (int i=0;i<epitopeStartI;i++) {
					newxs[i]=fx2.xs[i]/2+xs[i]/2;
				}
			}
			if (Random.nextDouble(0,1)<recombineEpitopes) {
				for (int i=epitopeStartI;i<Math.min(xs.length,fx2.xs.length);i++) {
					newxs[i]=Math.round(fx2.xs[i]/2+xs[i]/2);
				}
			}
		}
		
		if (calcNumStrains(newxs)>maxStrains || calcNumEpitopes(newxs)>maxEpitopes) {
			newxs=xs.clone();
		}

		return new Fx(populationSizeMultiplier,newxs,xmin,xmax);
	}
}