
public class BitManip {

	static long clear(long data, long from, long to) {
		long returnValue=data;
		for (long i=from;i<to;i++) {			
			returnValue|=(1<<i);
			returnValue-=(1<<i);
		}
		
		return returnValue;
	}
	
	static long set(long data, long bitnum) {				
		return data|(1<<bitnum);
	}
	
	static boolean get(long data, long bitnum) {				
		return (data&(1<<bitnum))>0;
	}
	
	static int get(long data, long from, long to) {				
		int returnValue=0;
		for (long i=from;i<to;i++) {			
			returnValue|=(data & (1<<i));			
		}
		return returnValue>>from;
	}
	
	static int nextSetBit(long data) {
		return Long.numberOfTrailingZeros(data);
	}
	
	static int cardinality(long data) {
		return Long.bitCount(data);
	}
	
	static String toString(long data) {
		return Long.toString(data,2);
	}
	

	
}
