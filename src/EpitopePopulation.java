/* A population of host individuals */


public class EpitopePopulation {

//	// LD strain infected and risk log;
//	static HashMap<Pair<Integer,Number>,Double> riskArray = null;
//	static HashMap<Pair<Integer,Number>,Integer> infectedArray = null;
//	static  HashMap<Pair<Integer,Number>,Integer> casesArray = null;
//
//		
//	EpitopePopulation() {
//		reset();
//	}
//
//	static void reset() {
//						
//		switch (Parameters.phenotypeSpace) {
//
//	
//		case LDLINEAR : {
//			// Generate list of all limited diversity phenotypes	
//			PhenotypeLimitedDiversityLinearXI p = new PhenotypeLimitedDiversityLinearXI(PhenotypeLimitedDiversityLinearXI.initialLimitedDiversityPhenotype);
//		
//			for (int i=0;i<p.numParts();i++) {				
//				for (int j=0;j<PhenotypeLimitedDiversityLinearXI.nStatesPerLoci[i];j++) {
//					riskArray.put(new Pair<Integer,Number>(i,j), 0.0);
//					infectedArray.put(new Pair<Integer,Number>(i,j), 0);
//					casesArray.put(new Pair<Integer,Number>(i,j), 0);
//				}
//				
//			}
//
//			break;
//		}
//		
//		case LDLINEAROLD : {
//			// Generate list of all limited diversity phenotypes	
//			PhenotypeLimitedDiversityLinearXI p = new PhenotypeLimitedDiversityLinearXI(PhenotypeLimitedDiversityLinearXI.initialLimitedDiversityPhenotype);
//		
//			for (Integer i=0;i<p.numParts();i++) {				
//				for (Integer j=0;j<PhenotypeLimitedDiversityLinearXI.nStatesPerLoci[i];j++) {
//					riskArray.put(new Pair<Integer,Number>(i,j), 0.0);
//					infectedArray.put(new Pair<Integer,Number>(i,j), 0);
//					casesArray.put(new Pair<Integer,Number>(i,j), 0);
//				}			
//			}
//
//			break;
//		}
//		
//		case LDSUNETRA : {
//			// Generate list of all limited diversity phenotypes
//			PhenotypeLimitedDiversitySunetra p = new PhenotypeLimitedDiversitySunetra(PhenotypeLimitedDiversitySunetra.initialLimitedDiversityPhenotype);
//			for (Integer i=0;i<p.numParts();i++) {				
//				for (Integer j=0;j<PhenotypeLimitedDiversityLinearXI.nStatesPerLoci[i];j++) {
//					riskArray.put(new Pair<Integer,Number>(i,j), 0.0);
//					infectedArray.put(new Pair<Integer,Number>(i,j), 0);
//					casesArray.put(new Pair<Integer,Number>(i,j), 0);
//				}			
//			}
//		}
//
//		default: break;
//		}	
//
//
//		
//	}
//
//	void add(Phenotype p) {
//		for (Integer i=0;i<p.numParts();i++) {	
//			Pair<Integer,Number> epitope = new Pair<Integer,Number>(i,p.getPhenotypePart(i));
//			infectedArray.put(epitope,infectedArray.get(epitope)+1);
//			casesArray.put(epitope,casesArray.get(epitope)+1);
//		}
//	}
//
//	void remove(Phenotype p) {
//		for (Integer i=0;i<p.numParts();i++) {	
//			Pair<Integer,Number> epitope = new Pair<Integer,Number>(i,p.getPhenotypePart(i));
//			infectedArray.put(epitope,infectedArray.get(epitope)-1);			
//		}
//	}
//
//	
//
//	public void printState(PrintStream printStream, int demeInfo) {
//		// TODO: Remove System.out...
//		
//		for (Pair<Integer,Number> e : riskArray.keySet()) {			
//			printStream.printf("%.4f\t%d\t%.4f\t%d\t%d\t%d\t\n",Parameters.getDate(),e,riskArray.get(e),infectedArray.get(e),casesArray.get(e),demeInfo);
//			System.out.printf("%.4f\t%d\t%.4f\t%d\t%d\t%d\t\n",Parameters.getDate(),e,riskArray.get(e),infectedArray.get(e),casesArray.get(e),demeInfo);
//		};
//
//	}
//	
//	public void clear() {
//		for (Pair<Integer,Number> epitope : riskArray.keySet()) {	
//			infectedArray.put(epitope,0);
//			casesArray.put(epitope,0);
//			riskArray.put(epitope, 0.0);			
//		}
//	}

//	
//
//	public void increaseRisk(Phenotype p, double d) {
//		for (Integer i=0;i<p.numParts();i++) {	
//			Pair<Integer,Number> epitope = new Pair<Integer,Number>(i,p.getPhenotypePart(i));
//			infectedArray.put(epitope,infectedArray.get(epitope)-1);			
//		}
//		
//	}
//
//	public void resetRiskAndCases() {
//		for (int i=0;i<riskArray.length;i++) {
//			riskArray[i]=0;
//			casesArray[i]=0;
//		}
//	}
//
//	public boolean casesInThisPeriod(Phenotype p)  {
//		return casesArray[i()]>0;
//	}
//
//	public int getNumPossibleStrains() {
//		// TODO Auto-generated method stub
//		return casesArray.length;
//	}




}

