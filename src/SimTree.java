import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.GregorianCalendar;


/* Implements an individual-based model in which the infection's genealogical history is tracked through time */

class SimTree {
	public static void main(String[] args) throws IOException {
		
		if (args.length>0) {
			if (args[0].equalsIgnoreCase("?") || args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-h")) {
				printHelp();
				return;
			}
		}

		// initialize random  number generator
		cern.jet.random.AbstractDistribution.makeDefaultGenerator();

		Parameters.applyArgsAndInit(args);
		Parameters.printParams();

		Simulation sim = new Simulation();	
		sim.reset();
		sim.run();
		
		try {
			File resultFile = new File("out.results");
			resultFile.delete();
			resultFile.createNewFile();
			PrintStream resultStream = new PrintStream(resultFile);
			
			resultStream.printf("diversity mean: %f, min %f, max %f num_significant_antigenic_variants: %f max_gap: %f inc: %f cv: %f\n",sim.getMeanDiversity(),sim.getMinDiversity(),sim.getMaxDiversity(), sim.getNumSignificantStrains(), sim.getMaxGapSize(), sim.getYearlyIncidancePercent(), sim.getPrevalenceCV());
			
			resultStream.print((new GregorianCalendar()).getTime());
												
			resultStream.close();
		} catch(IOException ex) {
			System.out.println("Could not write to file out.results!"); 
			System.exit(0);
		}
		
		System.out.printf("diversity mean: %f, min %f, max %f num_significant_antigenic_variants: %f max_gap: %f inc: %f",sim.getMeanDiversity(),sim.getMinDiversity(),sim.getMaxDiversity(), sim.getNumSignificantStrains(), sim.getMaxGapSize(), sim.getYearlyIncidancePercent());	
	}


	private static void printHelp() {	
		System.out.println("\n\nGeneral Parameters:");
		System.out.println("-------------------");
		Settings.printSettingsDescription(Parameters.class, System.out);
		Settings.printSettingsDescription(ImmuneSystem.class, System.out);
		System.out.println("\nGeometric Phenotype: (GEO)");
		Settings.printSettingsDescription(GeometricPhenotype.class, System.out);		
		System.out.println("\nEpochal Phenotype: (EPOCH)");
		Settings.printSettingsDescription(PhenotypeEpochal.class, System.out);
		System.out.println("\nEpochal Phenotype: (LDSUNETRA)");
		Settings.printSettingsDescription(PhenotypeLimitedDiversitySunetra.class, System.out);				
		System.out.println("\nLimited Diversity Linear Phenotype: (LDLINEAR)");
		Settings.printSettingsDescription(PhenotypeLimitedDiversityLinearXI.class, System.out);
		System.out.println("\nLimited Diversity Linear Phenotype (OLD): (LDLINEAROLD)");
		Settings.printSettingsDescription(PhenotypeLimitedDiversityLinearXIOLD.class, System.out);
		System.out.println("\nGeometeric 3D Phenotype: (GEO3D)");			
		Settings.printSettingsDescription(GeometricPhenotype3D.class, System.out);
		System.out.println("\n\nExamples:");
		System.out.println("--------");
		System.out.println("java -Xms2G -Xmx2G -jar simtree.jar phenotypeSpace=LDLINEAR defaultNoXImmunity=true random_key_params=false slope=0.1 beta=0.65 nu=0.20"); 	
		System.out.println("java -Xms12G -Xmx12G -jar simtree.jar initialNs=25000000,200000 demeNames=world,moon demeBaseLines=1,3 demeAmplitudes=0.2,0 demeOffsets=0,0 phenotypeSpace=LD random_key_params=false gamma=0.8 delta=0.1 beta=0.6 nu=0.20");
	}

	


}

