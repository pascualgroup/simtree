/* Antigenic phenotype present in individual Viruses and within Hosts as immune history */
/* Should be able to calculate distance and cross-immunity between two phenotypes */
/* Moving up to multiple dimensions is non-trivial and requires thought on the implementation */
/* Multiple Viruses can reference a single Phenotype object */


import java.util.BitSet;

public class PhenotypeLimitedDiversityLinearXIOLD  implements Phenotype {

	// DANIEL 
	// parameters specific to PhenotypeLimitedDiversityLinearXI

	@Setting static boolean defaultNoXImmunity = false;
	@Setting static double slope = 0.15;												
	@Setting static int nLoci = 5;	      
	static int numPossibleStrains = 1;       
	static final int[] multiplyForIndex = {1,2,6,24,120};
	@Setting static final int[] nStatesPerLoci =   {2,3,4,5,6};
	static final int[] initialLimitedDiversityPhenotype = {0,0,0,0,0};	
	public static enum PhenotypeMutationOptionType {Flipping, SingleTrait, NoMutation};
	@Setting public static PhenotypeMutationOptionType PhenotypeMutationOption = PhenotypeMutationOptionType.SingleTrait; 

	// fields		
	private int[] traits = null;
	private int index;

	// phenotype output
	static final boolean outputBinary = false;

	public static void init() {		

		numPossibleStrains=1;
		for (int i=0;i<nStatesPerLoci.length;i++) {
			numPossibleStrains*=nStatesPerLoci[i];
		}			

		if (Parameters.param_index>=0) {

			double[] slope_range = new double[Parameters.gridpoints];
			for (int i=0; i<Parameters.gridpoints; i++) {
				slope_range[i]=0+((double) i)/((double)Parameters.gridpoints-1.0);
			}

			slope = slope_range[Parameters.param_index%slope_range.length];

		}
		if (Parameters.random_key_params) {
			slope = Random.nextDouble(0, 1);
		}
	}

	// constructors
	public PhenotypeLimitedDiversityLinearXIOLD() {
		traits = new int[nLoci];

		for (int i=0;i<traits.length;i++) {
			traits[i]=0;
		}

		index=(-1);

	}

	public PhenotypeLimitedDiversityLinearXIOLD(int[] _traits) {			
		traits=_traits.clone();	
		index = (-1);
	}




	public int[] getTraits() {
		return traits;
	}

	// cross immunity between a virus phenotype and a host's immune history
	// (1-gamma) if any of the alleles has been encountered before
	// (1-gamma)x(1-delta) if all the alleles have been encountered before
	@Override
	public double riskOfInfection( ImmuneSystem immuneSystem) {
		
		ImmuneSystemPhenotypeHistoryBased history = (ImmuneSystemPhenotypeHistoryBased) immuneSystem;

		if (history.length() > 0) {

			BitSet hasEncounteredBefore = new BitSet(nLoci);

			for (int i = 0; i<history.length(); i++) {			
				hasEncounteredBefore.or(sharedAlleles((PhenotypeLimitedDiversityLinearXIOLD) history.get(i)));			
			}

			int numAllelesEncounteredBefore = hasEncounteredBefore.cardinality();
			int distance = nLoci - numAllelesEncounteredBefore; 

			double risk;
			if (defaultNoXImmunity) {
				risk = 1 - slope*numAllelesEncounteredBefore;
			}
			else {
				risk = slope*distance; 					
			}

			if (risk>1)return 1;
			else if (risk<0)return 0;					
			else return risk;
		}
		else {
			return 1;
		}
	}

	// returns loci of shared alleles
	private BitSet sharedAlleles(PhenotypeLimitedDiversityLinearXIOLD p) {
		BitSet returnValue = new BitSet(nLoci);		
		for (int i=0;i<nLoci;i++) {
			returnValue.set(i,traits[i]==p.traits[i]);
		}
		return returnValue;

	}

	// returns a random set of phenotype traits
	public static int[] randomTraits() {
		int[] returnValue = new int[nLoci];

		for (int i=0; i<nLoci;i++) {			
			returnValue[i]=Random.nextInt(0, nStatesPerLoci[i]-1); 		
		}

		return returnValue;
	}

	@Override
	public Phenotype mutate() {

		switch (PhenotypeMutationOption) {
		case Flipping:
			return new PhenotypeLimitedDiversityLinearXIOLD(randomTraits()); // returns a mutated version - this is basically a random version for this model

		case SingleTrait:
			// randoms one of the traits....

			int[] newTraits = traits.clone();

			int randLoci = Random.nextInt(0, nLoci-1);
			newTraits[randLoci] = Random.nextInt(0, nStatesPerLoci[randLoci]-1);

			return new PhenotypeLimitedDiversityLinearXIOLD(newTraits);

		case NoMutation: default: 
			return new PhenotypeLimitedDiversityLinearXIOLD(traits);		
		}



	}

	@Override
	public String toString() {
		return toString(",");
	}

	// returns allele version at each loci "3,2,....,allele_version_at_loci_n"
	@Override
	public String toString(String seperator) {

		String returnValue = "";

		if (outputBinary) {
			for (int i=0; i<nLoci; i++) {
				for (int j=0; j<nStatesPerLoci[i]; j++) {
					if (traits[i]==j) {
						returnValue = returnValue + "0";
					}
					else {
						returnValue = returnValue + "1";
					}
					if (j!=(nStatesPerLoci[i]-1)) {
						returnValue = returnValue +seperator;
					}
				}				 													
				if (i!=(nLoci-1)) {
					returnValue = returnValue + seperator;
				}
			}			
		}
		else {
			for (int i=0; i<nLoci; i++) {
				returnValue = returnValue + ((traits[i]-nStatesPerLoci[i]/2)); 														
				if (i!=(nLoci-1)) {
					returnValue = returnValue + seperator;
				}
			}			
		}

		return returnValue;
	}

	public boolean increaseTraits() {
		traits[0]=traits[0]+1;
		for (int i=0;i<(traits.length-1);i++) {
			if (traits[i]>=PhenotypeLimitedDiversityLinearXIOLD.nStatesPerLoci[i]) {
				traits[i]=0;
				traits[i+1]=traits[i+1]+1;
			}
		}
		if (traits[PhenotypeLimitedDiversityLinearXIOLD.nLoci-1]<PhenotypeLimitedDiversityLinearXIOLD.nStatesPerLoci[PhenotypeLimitedDiversityLinearXIOLD.nLoci-1]) {
			return true;
		} 
		else {
			for (int i=0;i<traits.length;i++) {
				traits[i]=0;
			}
			return false;
		}

	}

	public long getIndex() {

		if (index==(-1)) {

			int returnValue = 0;

			for (short i=0; i<nLoci; i++) {
				returnValue+=traits[i]*multiplyForIndex[i];
			}

			index = returnValue;

			return returnValue;
		}
		else {
			return index;
		}		
	}

	@Override
	public void add(ImmuneSystem immuneSystem) {
		immuneSystem.add(this);
		
	}
	
	@Override
	public Number getPhenotypePart(int n) {
		if ((n >= nLoci) || (n<0)) {
			System.err.println("only parts 0.."+nLoci+" in phenotype..."); return 0; 
		}
		else {
			return traits[n];
		}
	}
	
	@Override
	public int numParts() {
		return nLoci;
	}

	@Override
	public long getNumPossibleStrains() {		
		return numPossibleStrains;
	}



}
