import java.util.BitSet;


public class ImmuneSystemEpitopeNPhenotypeHistoryBased implements ImmuneSystem{
	
	BitSet immuneHistory = new BitSet(Parameters.defaultNumEpitopes);
	int lastInfectionTime = Integer.MIN_VALUE; // [days]
	//ArrayList<Short> phenotypeIDs = new ArrayList<Short>();
	//Set<Short> phenotypeIDs = new TreeSet<Short>();
	//Set<Short> phenotypeIDs = new ArrayList<Short>();
	//Set<Short> phenotypeIDs = new HashSet<Short>();
	
	@Setting static float fractionMemory = 1f; // Fraction of epitopes remembered (epitope based immune history only)	
	@Setting static boolean shortTermImmunity = false; // Immunity to all strains for a specific period following recovery
	@Setting static int shortTermImmunityLength = 120; // [days]
	@Setting public static byte MaxNumPreviousInfections = 20; // maximum number of previous infections  

	int[] phenotypeIDs = null;	
	byte phenotypeIDsLength = 0;
		
	ImmuneSystemEpitopeNPhenotypeHistoryBased() {
		phenotypeIDs = new int[MaxNumPreviousInfections];
	}
	
	public void reset() {
		immuneHistory.clear();		
		lastInfectionTime = Integer.MIN_VALUE;
		phenotypeIDsLength=0;
	}
	
	@Override
	public double riskOfInfection(Phenotype p) {
		if (shortTermImmunity==true) {
			if (Parameters.day>(lastInfectionTime+shortTermImmunityLength)) {
				return p.riskOfInfection(this);
			}
			else 
				return 0;
		}
		else {
			return p.riskOfInfection(this);			
		}
	}

	@Override
	public String printState() {
		
		String state = new String();		
		state = state + "Allocated Length: bits-" + immuneHistory.length()+"\n";
		state = state + immuneHistory + "\n";
		state = state + phenotypeIDs + "\n"; // probably need to fix this...
				
		return state;
	}

	public BitSet getHistory() {	
		return immuneHistory;
	}
	
	public boolean encounteredPhenotype(int phenotypeID) {
		for (byte i=0;i<phenotypeIDsLength;i++) {
			if (phenotypeIDs[i]==phenotypeID) 
				return true;
		}
		return false;
	}
	
	
	public void add( BitSet traits, int l ) {
		if (fractionMemory==1) {
			immuneHistory.or(traits);
		}
		else {			
			BitSet rememberedEpitopes = new BitSet(Parameters.defaultNumEpitopes);
			for (int i=0; i<traits.length(); i++) {
				if (traits.get(i)==true) {
					if (Random.nextBoolean(fractionMemory)) {
						rememberedEpitopes.set(i);
					}
				}			
			}
			immuneHistory.or(rememberedEpitopes);
		}
		//phenotypeIDs.add(phenotypeID);
		phenotypeIDs[(int)phenotypeIDsLength]=l;
		phenotypeIDsLength=(byte) (((byte)phenotypeIDsLength+(byte)1)%MaxNumPreviousInfections);

			
	}

	@Override
	public void add(Phenotype p) {
		if (shortTermImmunity==true) {
			lastInfectionTime=Parameters.day;
		}
		p.add(this);		
	}

	


	
	
	
	
}
