/* A population of host individuals */

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class HostPopulation {

	// fields
	private byte deme; // name
	private int cases;	// number of cases from last count, doesn't effect dynamics 
	private List<Host> susceptibles = new ArrayList<Host>();
	private List<Host> infecteds = new ArrayList<Host>();	
	//	private List<Host> recovereds = new ArrayList<Host>();		// this is the transcendental class, immune to all forms of virus  
	private Virus lastSurvivor = null;

	private PhenotypePopulation phenotypePopulation = null;
	// reused between demes

	float[] infected;
	float[] beta;
	float[][] contactsFrom;
	float[][] infectionsFrom;
	float[][] infectionRateFrom;
	float numSamples;

	// construct population, using Virus v as initial infection
	public HostPopulation(byte d) {
		// basic parameters
		deme = d;
		infected = new float[12];
		beta = new float[12];
		contactsFrom = new float[12][Parameters.initialNs.length];
		infectionsFrom = new float[12][Parameters.initialNs.length];
		infectionRateFrom = new float[12][Parameters.initialNs.length];
		for (int demeSamples : Parameters.tipSamplesPerDeme) {
			numSamples+=demeSamples;
		}
		reset();
	}

	// accessors
	public int getN() {
		return susceptibles.size() + infecteds.size();// + recovereds.size();
	}
	public int getS() {
		return susceptibles.size();
	}
	public int getI() {
		return infecteds.size();
	}
	//	public int getR() {
	//		return recovereds.size();
	//	}	
	public double getPrS() {
		return (double) getS() / (double) getN();
	}
	public double getPrI() {
		return (double) getI() / (double) getN();
	}
	//	public double getPrR() {
	//		return (double) getR() / (double) getN();
	//	}	
	public int getRandomN() {
		return Random.nextInt(0,getN()-1);
	}
	public int getRandomS() {
		return Random.nextInt(0,getS()-1);
	}
	public int getRandomI() {
		return Random.nextInt(0,getI()-1);
	}
	//	public int getRandomR() {
	//		return Random.nextInt(0,getR()-1);
	//	}

	public Host getRandomHost() {
		// figure out whether to pull from S, I or R
		Host h = null;
		double n = Random.nextDouble(0.0,1.0);
		if (n < getPrS()) {
			h = getRandomHostS();
		}
		else if (n > getPrS() && n < getPrS() + getPrI()) {
			h = getRandomHostI();
		}
		//		else if (n > getPrS() + getPrI()) {
		//			h = getRandomHostR();
		//		}
		return h;
	}

	public Host getRandomHostS() {
		int index = Random.nextInt(0,getS()-1);
		return susceptibles.get(index);
	}
	public Host getRandomHostI() {
		Host h = null;
		if (getI() > 0) {
			int index = Random.nextInt(0,getI()-1);
			h = infecteds.get(index);
		}
		return h;
	}
	//	public Host getRandomHostR() {
	//		Host h = null;
	//		if (getR() > 0) {	
	//			int index = Random.nextInt(0,getR()-1);
	//			h = recovereds.get(index);
	//		}
	//		return h;
	//	}	

	public void resetCases() {
		cases = 0;
	}

	public int getCases() {
		return cases;
	}	

	public void removeSusceptible(int i) {
		susceptibles.set(i, susceptibles.get(susceptibles.size()-1));
		susceptibles.remove(susceptibles.size()-1);
	}	
	public void removeInfected(int i) {
		phenotypePopulation.remove(infecteds.get(i).getInfection().getPhenotype());
		infecteds.set(i, infecteds.get(infecteds.size()-1));
		infecteds.remove(infecteds.size()-1);

	}
	//	public void removeRecovered(int i) {
	//		recovereds.set(i, recovereds.get(recovereds.size()-1));
	//		recovereds.remove(recovereds.size()-1);
	//	}	

	public void stepForward() {

		//	resetCases();
		if (Parameters.swapDemography) {
			swap();
		} else {
			grow();
			decline();
		}
		contact();
		recover();

		if (!Parameters.mutateOnlyDuringBurnin || (Parameters.day<Parameters.burnin)) {
			mutate();
		}
		sample(); // doesn't effect dynamics

	}

	// draw a Poisson distributed number of births and add these hosts to the end of the population list
	public void grow() {
		double totalBirthRate = getN() * Parameters.birthRate;
		int births = Random.nextPoisson(totalBirthRate);
		for (int i = 0; i < births; i++) {
			Host h = new Host();
			susceptibles.add(h);
		}
	}

	// draw a Poisson distributed number of deaths and remove random hosts from the population list
	public void decline() {
		// deaths in susceptible class
		double totalDeathRate = getS() * Parameters.deathRate;
		int deaths = Random.nextPoisson(totalDeathRate);
		for (int i = 0; i < deaths; i++) {
			if (getS()>0) {
				int sndex = getRandomS();
				removeSusceptible(sndex);
			}
		}		
		// deaths in infectious class		
		totalDeathRate = getI() * Parameters.deathRate;
		deaths = Random.nextPoisson(totalDeathRate);
		for (int i = 0; i < deaths; i++) {

			if (getI()>0) {
				int index = getRandomI();
				if (!Parameters.keepStrainsAlive || !phenotypePopulation.lastOfItsKind(infecteds.get(index).getInfection().getPhenotype())) {
					if (getI()==1) {
						lastSurvivor=infecteds.get(0).getInfection();
					}
					removeInfected(index);
				}


			}
		}		
	}

	// draw a Poisson distributed number of births and reset these individuals
	public void swap() {
		// draw random individuals from susceptible class
		double totalBirthRate = getS() * Parameters.birthRate;
		int births = Random.nextPoisson(totalBirthRate);
		for (int i = 0; i < births; i++) {
			if (getS()>0) {
				int index = getRandomS();
				Host h = susceptibles.get(index);
				h.reset();
			}
		}		
		// draw random individuals from infected class
		totalBirthRate = getI() * Parameters.birthRate;
		births = Random.nextPoisson(totalBirthRate);


		for (int i = 0; i < births; i++) {
			if (((Parameters.day>Parameters.burnin) || (!Parameters.keepAliveDuringBurnin)) && (!Parameters.keepAlive)) {
				if (getI()>0) {
					int index = getRandomI();
					Host h = infecteds.get(index);					
					if (getI()==1) {
						lastSurvivor=infecteds.get(0).getInfection();
					}
					if (!Parameters.keepStrainsAlive || !phenotypePopulation.lastOfItsKind(h.getInfection().getPhenotype())) {
						removeInfected(index);
						h.reset();					
						susceptibles.add(h);
					}

				}
			}
			else if ((Parameters.day<Parameters.burnin || Parameters.keepAlive) && getI()>1) {
				int index = getRandomI();
				Host h = infecteds.get(index);
				if (getI()==1) { // not for dynamics, for tree reconstruction
					lastSurvivor=infecteds.get(0).getInfection();
				}
				removeInfected(index);
				h.reset();
				susceptibles.add(h);
			}

		}	
		// draw random individuals from recovered class
		//		totalBirthRate = getR() * Parameters.birthRate;
		//		births = Random.nextPoisson(totalBirthRate);
		//		for (int i = 0; i < births; i++) {
		//			if (getR()>0) {
		//				int index = getRandomR();
		//				Host h = recovereds.get(index);
		//				h.reset();
		//				removeRecovered(index);
		//				susceptibles.add(h);
		//			}
		//		}			
	}

	// draw a Poisson distributed number of contacts and move from S->I based upon this
	public void contact() {

		// each infected makes I->S contacts on a per-day rate of beta * S/N
		double totalContactRate = getI() * getPrS() * Parameters.beta * Parameters.getSeasonality(deme);
		int contacts = Random.nextPoisson(totalContactRate);
		for (int i = 0; i < contacts; i++) {
			if (getS()>0 && getI()>0) {

				// get indices and objects
				int index = getRandomI();
				int sndex = getRandomS();
				Host iH = infecteds.get(index);
				Host sH = susceptibles.get(sndex);			
				Virus v = iH.getInfection();

				// attempt infection
				Phenotype p = v.getPhenotype();

				double chanceOfSuccess = sH.riskOfInfection(p); 
				if (Random.nextBoolean(chanceOfSuccess)) {
					sH.infect(v,deme);
					removeSusceptible(sndex);
					phenotypePopulation.add(v.getPhenotype()); // doesn't effect dynamics
					infecteds.add(sH);
					cases++; // doesn't effect dynamics
				}


			}
		}		

	}

	// draw a Poisson distributed number of contacts and move from S->I based upon this
	// this deme is susceptibles and other deme is infecteds
	public void betweenDemeContact(HostPopulation other) {
		double totalContactRate = 0;
		switch (Parameters.betweenDemeContactType) {
		case PROPORTIONAL:		
			// each infected makes I->S contacts on a per-day rate of beta * S/N
			totalContactRate = other.getI() * getPrS() * Parameters.beta * Parameters.betweenDemePro[other.deme][deme] * Parameters.getSeasonality(other.deme);
			break;
		case TEST_STEP:
			if (Parameters.day%365<=182) {
				totalContactRate=Parameters.constantContactRate[other.deme][deme]/3;
			}
			else {
				totalContactRate=Parameters.constantContactRate[other.deme][deme];
			}
			break;
		case CONSTANT:
			totalContactRate=Parameters.constantContactRate[other.deme][deme];
			break;			
		}
		int contacts = Random.nextPoisson(totalContactRate);
		int month = (int) Math.floor((Parameters.getDate()%1.0)*12.0);
		if (Parameters.day > Parameters.burnin) {
			contactsFrom[month][other.deme]+=contacts;
			infected[month]+=(getI()/365.0*12.0);
			beta[month]+=(Parameters.beta*Parameters.getSeasonality(deme)/365.0*12.0);
		}
		for (int i = 0; i < contacts; i++) {
			if (getS()>0 && other.getI()>0) {
				// get indices and objects
				Host iH = other.getRandomHostI();
				int sndex = getRandomS();
				Host sH = susceptibles.get(sndex);			
				Virus v = iH.getInfection();

				// attempt infection
				Phenotype p = v.getPhenotype();

				double chanceOfSuccess = sH.riskOfInfection(p);
				if (Random.nextBoolean(chanceOfSuccess)) {
					sH.infect(v,deme);
					phenotypePopulation.add(p);
					removeSusceptible(sndex);					
					infecteds.add(sH);
					cases++;
					if (Parameters.day > Parameters.burnin) {
						infectionsFrom[month][other.deme]+=1;
						infectionRateFrom[month][other.deme]+=1.0/((float)other.getI())/365.0*12.0; 
					}
				}
			}
		}		
	}	

	// draw a Poisson distributed number of recoveries and move from I->S based upon this
	public void recover() {
		// each infected recovers at a per-day rate of nu
		double totalRecoveryRate = getI() * Parameters.nu;
		int recoveries = Random.nextPoisson(totalRecoveryRate);

		if (!Parameters.keepStrainsAlive) { 
			for (int i = 0; i < recoveries; i++) {
				if (((Parameters.day>Parameters.burnin) || (!Parameters.keepAliveDuringBurnin)) && (!Parameters.keepAlive)) {
					if (getI()>0) {
						int index = getRandomI();
						Host h = infecteds.get(index);
						if (getI()==1) {
							lastSurvivor=infecteds.get(0).getInfection();
						}
						removeInfected(index);
						h.clearInfection();
						susceptibles.add(h);
					}
				}
				else if ((Parameters.day<Parameters.burnin || Parameters.keepAlive) && getI()>1) {
					int index = getRandomI();
					Host h = infecteds.get(index);
					if (getI()==1) {
						lastSurvivor=infecteds.get(0).getInfection();
					}
					removeInfected(index);

					h.clearInfection();					
					susceptibles.add(h);
				}


			}
		}
		else {
			for (int i = 0; i < recoveries; i++) {
				if (getI()>0) {
					int index = getRandomI();
					Host h = infecteds.get(index);
					if (getI()==1) {
						lastSurvivor=infecteds.get(0).getInfection();
					}

					if (!phenotypePopulation.lastOfItsKind(h.getInfection().getPhenotype())) {
						removeInfected(index);
						h.clearInfection();						
						susceptibles.add(h);
					}
				}
			}
		}
	}			


	// draw a Poisson distributed number of R->S 
	//	public void loseImmunity() {
	//		// each recovered regains immunity at a per-day rate
	//		double totalReturnRate = getR() * Parameters.immunityLoss;
	//		int returns = Random.nextPoisson(totalReturnRate);
	//		for (int i = 0; i < returns; i++) {
	//			if (getR()>0) {
	//				int index = getRandomR();
	//				Host h = recovereds.get(index);
	//				removeRecovered(index);
	//				susceptibles.add(h);
	//			}
	//		}			
	//	}	

	// draw a Poisson distributed number of mutations and mutate based upon this
	// mutate should not impact other Virus's Phenotypes through reference
	public void mutate() {

		// each infected mutates at a per-day rate of mu
		double totalMutationRate = getI() * Parameters.muPhenotype;
		int mutations = Random.nextPoisson(totalMutationRate);
		for (int i = 0; i < mutations; i++) {
			if (getI()>0) {
				int index = getRandomI();
				Host h = infecteds.get(index);
				phenotypePopulation.remove(h.getInfection().getPhenotype());			// doesn't effect dynamics					
				h.mutate();
				phenotypePopulation.add(h.getInfection().getPhenotype()); 	// doesn't effect dynamics
			}
		}			
	}	

	// draw a Poisson distributed number of samples and add them to the VirusSample
	// only sample after burnin is completed
	public void sample() {
		if (getI()>0 && Parameters.day >= Parameters.burnin) {

			double totalSamplingRate = Parameters.tipSamplingRate;
			if (Parameters.tipSamplingProportional) {
				totalSamplingRate *= getI();
			} 

			int samples = Random.nextPoisson(totalSamplingRate);
			for (int i = 0; i < samples; i++) {
				int index = getRandomI();
				Host h = infecteds.get(index);
				Virus v = h.getInfection();
				VirusTree.add(v);
			}	
		}
	}

	// through current infected population assigning ancestry as trunk
	public void makeTrunk() {
		for (int i = 0; i < getI(); i++) {
			Host h = infecteds.get(i);
			Virus v = h.getInfection();
			v.makeTrunk();
			while (v.getParent() != null) {
				v = v.getParent();
				if (v.isTrunk()) {
					break;
				} else {
					v.makeTrunk();
				}
			}
		}

		if ((lastSurvivor!=null) && (Parameters.day<Parameters.endDay) ) {
			Virus v = lastSurvivor;
			v.makeTrunk();
			while (v.getParent() != null) {
				v = v.getParent();
				if (v.isTrunk()) {
					break;
				} else {
					v.makeTrunk();
				}
			}		
		}

	}	

	public void printState(PrintStream stream) {
		if (Parameters.day > Parameters.burnin) {
			stream.printf("\t%d\t%d\t%d\t%d\t%d", getN(), getS(), getI(),0 /* getR()*/, getCases());
		}
	}	

	// reset population to factory condition
	public void reset() {

		// clearing lists
		susceptibles.clear();
		infecteds.clear();
		phenotypePopulation = new PhenotypePopulation(Parameters.initialNs[deme]);
		phenotypePopulation.clear(); // doesn't effect dyanamics
		//	recovereds.clear();

		//	int initialR = 0;

		// fill population with susceptibles
		int initialS = Parameters.initialNs[deme];/* - initialR;*/
		if (deme == 1) {
			initialS -= Parameters.initialI; // minus initial number of infected
		}
		if (!Parameters.silent) {
			System.out.println("\nDeme " + deme);
			System.out.println("-----------");
		}
		for (int i = 0; i < initialS; i++) {
			if (i%5000000 == 0 && !Parameters.silent) {
				System.out.println("adding hosts: " + i + " out of " + initialS);		
			}
			Host h = new Host();			
			susceptibles.add(h);
		}
		if (!Parameters.silent)
			System.out.println("adding hosts: " + initialS + " out of " + initialS);

		// fill population with recovereds
		//	for (int i = 0; i < initialR; i++) {
		//		Host h = new Host();			
		//		recovereds.add(h);
		//	}		

		if (deme == 1) { // only the X deme is filled with an initial infected population
			// infect some individuals
			for (int i = 0; i < Parameters.initialI; i++) {
				Virus v = new Virus(Parameters.urVirus, deme);
				Host h = new Host(v);
				infecteds.add(h);
				phenotypePopulation.add(v.getPhenotype()); // doesn't effect dynamics, for record out.immunity
			}	
		}

		// fill population with "recoverds" = susceptibles with immune memory
		for (int i = 0; i < Parameters.initialPrR*Parameters.initialNs[deme]; i++) {
			if (i%5000000 == 0 && !Parameters.silent) {
				System.out.println("generating random host immune history: " + i + " out of " + Parameters.initialPrR*Parameters.initialNs[deme]);		
			}			
			Host h = getRandomHost();			
			h.addToImmuneHistory(PhenotypeFactory.makeHostPhenotype());
		}	
		if (!Parameters.silent)
			System.out.println("generating random host immune history: " + Parameters.initialPrR*Parameters.initialNs[deme] + " out of " + Parameters.initialPrR*Parameters.initialNs[deme]);
	}

	public List<Host> getIs() {	
		return infecteds;
	}

	public PhenotypePopulation getPhenotypePopulation() {
		return phenotypePopulation;
	}

	public void setPhenotypePopulation(PhenotypePopulation phenotypePopulation) {
		this.phenotypePopulation = phenotypePopulation;
	}

}