



public class ImmuneSystemPhenotypeHistoryBased implements ImmuneSystem{
	
	Phenotype[] immuneHistory = new Phenotype[0];
	private int length = 0;
	private int lastInfectionTime = Integer.MIN_VALUE;
	static final private int memAllocationQuanta = 4;
	static final private int resetCleanupThreshold = 8;
	
	@Setting static boolean shortTermImmunity = false; // Immunity to all strains for a specific period following recovery
	@Setting static int shortTermImmunityLength = 120; // [days]	

	
	public void reset() {
		if (length>resetCleanupThreshold) {
			immuneHistory = new Phenotype[0];
		}
		length = 0;
		lastInfectionTime = Integer.MIN_VALUE;
	}
	
	public int length() {
		return length;
	}

	public void add( Phenotype p ) {
		if (immuneHistory.length==length) {
			Phenotype[] newImmuneHistory = new Phenotype[immuneHistory.length+memAllocationQuanta];
			System.arraycopy(immuneHistory, 0, newImmuneHistory, 0, immuneHistory.length);
			newImmuneHistory[immuneHistory.length]=p;
			immuneHistory = newImmuneHistory;
		}
		immuneHistory[length]=p;
		length=length+1;
	}
	
	public Phenotype get ( int i ) {
		return immuneHistory[i];
	}
	
	public Phenotype[] getPhenotypes () {
		return immuneHistory;
	}

	@Override
	public double riskOfInfection(Phenotype p) {
		if (shortTermImmunity==true) {
			if (Parameters.day>(lastInfectionTime +shortTermImmunityLength)) {
				return p.riskOfInfection(this);
			}
			else 
				return 0;
		}
		else {
			return p.riskOfInfection(this);			
		}
	}

	@Override
	public String printState() {
		
		String state = new String();
		state = state + "Allocated Length: " + immuneHistory.length;
		state = state + " Used Length: " + length + "\n";
		for (int i = 0; i < length; i++) {
			state = state + "History phenotype: " + immuneHistory[i] + "\n";
		}
		
		return state;
	}

	
	
	
}
