/* Simulation functions, holds the host population */

import java.util.*;
import java.io.*;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class Simulation {
	
	

	// fields
	private List<HostPopulation> demes = new ArrayList<HostPopulation>();
	private double diversity;
	private double minDiversity = Parameters.endDay/365;
	private double maxDiversity = 0;
	private double meanDiversity = 0;
	private int countDiversity = 0;
	private int gapStart = Parameters.burnin;
	private int currentgap = 0;
	private int maxgap = 0;	
	private int totalCases=0;
	private double minInfectedP = 1;
	private double gapThresholdP = 0.00005;
	
	private SummaryStatistics myStats = new SummaryStatistics();
	
	
	// constructor
	public Simulation() {
		
	}

	// methods

	public int getN() {
		int count = 0;
		for (int i = 0; i < Parameters.initialNs.length; i++) {
			HostPopulation hp = demes.get(i);
			count += hp.getN();
		}
		return count;
	}

	public int getS() {
		int count = 0;
		for (int i = 0; i < Parameters.initialNs.length; i++) {
			HostPopulation hp = demes.get(i);
			count += hp.getS();
		}
		return count;
	}	

	public int getI() {
		int count = 0;
		for (int i = 0; i < Parameters.initialNs.length; i++) {
			HostPopulation hp = demes.get(i);
			count += hp.getI();
		}
		return count;
	}	

	//	public int getR() {
	//		int count = 0;
	//		for (int i = 0; i < Parameters.initialNs.length; i++) {
	//			HostPopulation hp = demes.get(i);
	//			count += hp.getR();
	//		}
	//		return count;
	//	}		

	public int getCases() {
		int count = 0;
		for (int i = 0; i < Parameters.initialNs.length; i++) {
			HostPopulation hp = demes.get(i);
			count += hp.getCases();
		}
		return count;
	}	

	public double getDiversity() {
		return diversity;
	}	
	
	public double getNumSignificantStrains() {
		return demes.get(0).getPhenotypePopulation().getNumSignificantStrains();
	}	
	
	public double getMinDiversity() {
		return minDiversity;
	}	
	
	public double getMaxDiversity() {
		return maxDiversity;
	}

	// proportional to infecteds in each deme
	public int getRandomDeme() {
		int n = Random.nextInt(0,getN()-1);
		byte d = 0;
		int target = (demes.get(0)).getN();
		while (n < target) {
			d += 1;
			target += (demes.get(d)).getN();
		}
		return d;
	}

	// return random virus proportional to worldwide prevalence
	public Virus getRandomInfection() {

		Virus v = null;

		if (getI() > 0) {

			// get deme proportional to prevalence
			int n = Random.nextInt(0,getI()-1);
			byte d = 0;
			int target = (demes.get(0)).getI();
			while (d < Parameters.initialNs.length) {
				if (n < target) {
					break;
				} else {
					d++;
					target += (demes.get(d)).getI();
				}	
			}
			HostPopulation hp = demes.get(d);

			// return random infection from this deme
			if (hp.getI()>0) {
				Host h = hp.getRandomHostI();
				v = h.getInfection();
			}

		}

		return v;

	}

	// return random host from random deme
	public Host getRandomHost() {
		byte d = Random.nextByte((byte) 0,(byte) (Parameters.initialNs.length-1));
		HostPopulation hp = demes.get(d);
		return hp.getRandomHost();
	}

	public double getAverageRisk(Phenotype p) {
			
		double hostsForImmunityReeconstruction = Math.round(Parameters.percentHostsForImmunityReconstruction*getN()/100.0);
		double averageRisk = 0;
		
		for (int i = 0; i < hostsForImmunityReeconstruction; i++) {
			Host h = getRandomHost();			
			averageRisk += h.riskOfInfection(p);
		}
		averageRisk /= hostsForImmunityReeconstruction;
		return averageRisk;
	}

	
	public void printImmunity() {
		if (Parameters.phenotypeSpace==PhenotypeType.GEO) {
			try {

				File immunityFile = new File("out.immunity");
				immunityFile.delete();
				immunityFile.createNewFile();
				PrintStream immunityStream = new PrintStream(immunityFile);

				for (double x = VirusTree.xMin; x <= VirusTree.xMax; x += 0.5) {
					for (double y = VirusTree.yMin; y <= VirusTree.yMax; y += 0.5) {

						Phenotype p = PhenotypeFactory.makeArbitaryPhenotype(x,y);
						double risk = getAverageRisk(p);
						immunityStream.printf("%.4f,", risk);

					}
					immunityStream.println();
				}

				immunityStream.close();
			} catch(IOException ex) {
				System.out.println("Could not write to file"); 
				System.exit(0);
			}
		}

	}

	public void makeTrunk() {
		for (int i = 0; i < Parameters.initialNs.length; i++) {
			HostPopulation hp = demes.get(i);
			hp.makeTrunk();
		}
	}	

	public void printState() {
		System.out.printf("%d\t%.3f\t%d\t%d\t%d\t%d\t%d\n", Parameters.day, getDiversity(), getN(), getS(), getI(),0 /* getR() */, getCases());

		
	}

	public void printHeader(PrintStream stream) {
		stream.print("date\tdiversity\ttotalN\ttotalS\ttotalI\ttotalR\ttotalCases");
		for (int i = 0; i < Parameters.initialNs.length; i++) {
			String name = Parameters.demeNames[i];
			stream.printf("\t%sN\t%sS\t%sI\t%sR\t%sCases", name, name, name, name, name);
		}
		stream.println();
	}

	public void printState(PrintStream stream) {
		if (Parameters.day > Parameters.burnin) {
			stream.printf("%.4f\t%.4f\t%d\t%d\t%d\t%d\t%d", Parameters.getDate(),getDiversity(), getN(), getS(), getI(), 0/* getR()*/, getCases());
			for (int i = 0; i < Parameters.initialNs.length; i++) {
				HostPopulation hp = demes.get(i);
				hp.printState(stream);
			}
			totalCases+=getCases();
			stream.println();
		}
	}	


	public void updateDiversityGapCheckAndTotalCases() {
		
		myStats.addValue(getI());
		
		// Min Infected Count
		if (getI()<(getN()*minInfectedP) && Parameters.day>Parameters.burnin) {
			minInfectedP=((double) getI())/((double) getN());
		}
			
		// Gap Check
		if (getI() < (getN()*gapThresholdP) && Parameters.day>=Parameters.burnin) {
			currentgap=Parameters.day-gapStart;
		}
		
		if (currentgap>maxgap) {
			maxgap=currentgap;
		}
		
		if (getI() >= (getN()*gapThresholdP) && Parameters.day>=Parameters.burnin) {
			gapStart=Parameters.day;
		}

		// Diversity
		diversity = 0.0;
		int sampleCount = Parameters.diversitySamplingCount;
		for (int i = 0; i < sampleCount; i++) {
			Virus vA = getRandomInfection();
			Virus vB = getRandomInfection();
			if (vA != null && vB != null) {
				diversity += vA.distance(vB);
			}
		}	
		diversity /= (double) sampleCount;
		
		if (diversity>maxDiversity && Parameters.day>Parameters.burnin) {
			maxDiversity=diversity;
		}
		if (diversity<minDiversity && Parameters.day>Parameters.burnin) {
			minDiversity=diversity;
		}
		
		if (Parameters.day>Parameters.burnin) {
			meanDiversity = (meanDiversity*countDiversity+diversity)/(countDiversity+1);
			countDiversity +=1;
		}
	}	

	public void resetCases() {
		for (int i = 0; i < Parameters.initialNs.length; i++) {	
			HostPopulation hp = demes.get(i);
			hp.resetCases();
		}
	}

	public void stepForward() {

		for (int i = 0; i < Parameters.initialNs.length; i++) {		
			HostPopulation hp = demes.get(i);
			hp.stepForward(); // within deme dyanmics
			for (int j = 0; j < Parameters.initialNs.length; j++) { // between deme dynamics
				if (i != j) {
					HostPopulation hpOther = demes.get(j);
					hp.betweenDemeContact(hpOther);
				}
			}
		}

		Parameters.day++;

	}

	public void run() throws IOException {

		try {

			File seriesFile = new File("out.timeseries");		
			seriesFile.delete();
			seriesFile.createNewFile();
			PrintStream seriesStream = new PrintStream(seriesFile);
			if (!Parameters.silent)
				System.out.println("day\t\tdiversity\tN\tS\tI\tR\tcases");
			printHeader(seriesStream);

			File immunityFile = null;
			PrintStream immunityStream = null;
			// LD Immunity file
			if (Parameters.phenotypeSpace==PhenotypeType.LD || Parameters.phenotypeSpace==PhenotypeType.LDSUNETRA || Parameters.phenotypeSpace==PhenotypeType.LDLINEAR || Parameters.phenotypeSpace==PhenotypeType.LDLINEAROLD) {
				if (Parameters.immunityReconstruction) {
					immunityFile = new File("out.immunity");
					immunityFile.delete();
					immunityFile.createNewFile();
					immunityStream = new PrintStream(immunityFile);
					printImmunityHeader(immunityStream);
					//					resetInfectedPerLDStrain();
				}
			}

			for (int i = 0; i < Parameters.endDay; i++) {
				
				stepForward(); // population dynamics

				if (Parameters.day % Parameters.printStepTimeseries == 0) { // output
					updateDiversityGapCheckAndTotalCases();
					if (!Parameters.silent) printState();
					printState(seriesStream);
					resetCases();
				}

				if (Parameters.day % Parameters.printStepImmunity == 0) {
					// LD immunity
					if (Parameters.phenotypeSpace ==PhenotypeType.LD || Parameters.phenotypeSpace ==PhenotypeType.LDSUNETRA || Parameters.phenotypeSpace == PhenotypeType.LDLINEAR || Parameters.phenotypeSpace == PhenotypeType.LDLINEAROLD) {
						if (Parameters.immunityReconstruction) {
							printLDImmunity(immunityStream);
						}
					}
				}

				
				if (getI()==0) {
					if (Parameters.repeatSim) {
						reset();
						i = 0; 
						seriesFile.delete();
						seriesFile.createNewFile();						
						if (Parameters.phenotypeSpace==PhenotypeType.LD || Parameters.phenotypeSpace==PhenotypeType.LDSUNETRA || Parameters.phenotypeSpace==PhenotypeType.LDLINEAR || Parameters.phenotypeSpace == PhenotypeType.LDLINEAROLD) {
							if (Parameters.immunityReconstruction) {
								immunityFile.delete();
								immunityFile.createNewFile();
								immunityStream = new PrintStream(immunityFile);
								printImmunityHeader(immunityStream);								
								//								resetInfectedPerLDStrain();
							}
						}					
						seriesStream = new PrintStream(seriesFile);
						printHeader(seriesStream);
						
						
						
						
					} else {
						break;
					}
				}
			}

			seriesStream.close();
		} catch(IOException ex) {
			System.out.println("Could not write to file"); 
			System.exit(0);
		}	

		// tree reduction
		VirusTree.pruneTips(); 
		VirusTree.markTips();		

		// tree prep
		makeTrunk();
		VirusTree.fillBackward();	
		
		VirusTree.sortChildrenByDescendants();
	//	VirusTree.pruneTips();
		
		// simulation seasonal stat
		printSeasonalStats();	
				
		VirusTree.streamline();
		VirusTree.setLayoutByDescendants();
		
			

		// rotation
		VirusTree.rotate();
		VirusTree.flip();

		// tip and tree output
		VirusTree.printTips();			
		VirusTree.printBranches();	
		
		// mk output
		VirusTree.printMK();
		
		
		// immunity output
				if (Parameters.phenotypeSpace ==PhenotypeType.GEO) {
					VirusTree.updateRange();
					VirusTree.printRange();
					if (Parameters.immunityReconstruction) {
						printImmunity();
					}
				}	
		
		// nexus
		VirusTree.collapseStrongForward(); 
		VirusTree.printNexus(false);
		VirusTree.printNexus(true);	

	}


	private void printImmunityHeader(PrintStream immunityStream) {
				
		immunityStream.print("year\tphenotype_number\trisk_of_infection\tinfected\tcases\tdeme");
		
		if (!Parameters.silent) 
			System.out.print("year\tphenotype_number\trisk_of_infection\tinfected\tcases\tdeme");
		
		for (int i=0; i<VirusTree.getRoot().getPhenotype().numParts(); i++) {
			immunityStream.print("\tloci" + i );
			if (!Parameters.silent) 
				System.out.print("\tloci" + i );
		}
		immunityStream.println();

	}

	private void printLDImmunity(PrintStream printStream) {
		
		// Run once one printStepImmunity before burnin ...
		if (Parameters.day>(Parameters.burnin-Parameters.printStepImmunity) && (Parameters.day<Parameters.burnin)) {
			for (int i=0; i<Parameters.initialNs.length; i++) {				
				resetRiskAndCasesPerLDStrain(i);
			}
		}			
		
		// Run every time printLDimmunity is called
		if (Parameters.day > Parameters.burnin) {
					
			// Go over iniitalNs.length demes
			for (int i=0; i<Parameters.initialNs.length; i++) {
				// Calculate risk for each deme
				calculateRiskPerLDStrain(i);
				// Print risk for each deme
				demes.get(i).getPhenotypePopulation().printState(printStream,i);
				// Reset risk for all strains
				resetRiskAndCasesPerLDStrain(i);
			}
			
		}	

	}

	private void calculateRiskPerLDStrain(int deme) {

		// Sample 10000 hosts in each deme for risk of infection calculation 	
		double hostsForImmunityReeconstruction = Math.round(Parameters.percentHostsForImmunityReconstruction*getN()/100.0);
		
		
		for (Phenotype p : demes.get(deme).getPhenotypePopulation().getTrackedStrains()) {
			if (demes.get(deme).getPhenotypePopulation().casesInThisPeriod(p)) {
				for (int i = 0; i <  hostsForImmunityReeconstruction; i++) {
					Host h = demes.get(deme).getRandomHost();							
					demes.get(deme).getPhenotypePopulation().increaseRisk(p, h.riskOfInfection(p)/hostsForImmunityReeconstruction);
				}
			}
		}		


	}
	
	private void resetRiskAndCasesPerLDStrain(int deme) {

		// Sample 10000 hosts in each deme for risk of infection calculation 		

		demes.get(deme).getPhenotypePopulation().resetRiskAndCases();

	}

	

	public void reset() {
		Parameters.init();
		VirusTree.init();	
//		diversity = 0;
		if (demes.size()>0) {
			for (int i = 0; i < Parameters.initialNs.length; i++) {
				HostPopulation hp = demes.get(i);
				hp.reset();		
			}
		}
		else {
			for (byte i = 0; i < Parameters.initialNs.length; i++) {
				HostPopulation hp = new HostPopulation(i);
				demes.add(hp);
			}	
		}
		diversity =0;
		minDiversity = Parameters.endDay/365;
		maxDiversity = 0;
		meanDiversity = 0;
		countDiversity = 0;
		gapStart = Parameters.burnin;
		currentgap = 0;
		maxgap = 0;	
		totalCases=0;
		minInfectedP=1;
		gapThresholdP = 0.001;
	}

	public double getMeanDiversity() {		
		return meanDiversity;
	}
		
	public double getMaxGapSize() {		
		return maxgap/365.0;
	}
	
	public double getPrevalenceCV() {
		
		return myStats.getStandardDeviation()/myStats.getMean();
	}
	
	public double getYearlyIncidancePercent() {	
		
		double popSize = 0;
		for (int i=0; i<demes.size();i++){
			popSize+=demes.get(i).getN();			
		}		
		return ((double)totalCases)*365.0/(Parameters.endDay-Parameters.burnin)/popSize*100;
	}

	public double getMinInfectedP() {
		return minInfectedP;
	}
	
	public void printSeasonalStats() throws IOException {
		
		// TODO: Inconsistent division by simulation length 
		double[][][] transitions = VirusTree.getTransitions();
		double[][] extantLineages = VirusTree.getNumberExtantLineages();
				
		File seasFile = new File("out.seasonalstats");		
		seasFile.delete();
		seasFile.createNewFile();
		PrintStream seasStream = new PrintStream(seasFile);
		String header = "parameter\tmonth\tvalue\tfrom\tto\tunits\n";
		seasStream.print(header);
		double numYears=(float)(Parameters.day-Parameters.burnin)/365.0;
		for (int i=0;i<demes.size();i++) {
			for (int j=0;j<12;j++) {
				seasStream.print("beta\t"+j+"\t"+demes.get(i).beta[j]/numYears+"\t"+i+"\tunitless\n");
			}
		}
		
		for (int i=0;i<demes.size();i++) {
			for (int j=0;j<12;j++) {
				seasStream.print("infected\t"+j+"\t"+demes.get(i).infected[j]/numYears+"\t"+i+"\tindividuals\n");
			}
		}
		
		for (int i=0;i<demes.size();i++) {
			for (int j=0;j<demes.size();j++) {
				for (int k=0;k<12;k++) {
					seasStream.print("contacts\t"+k+"\t"+demes.get(i).contactsFrom[k][j]/numYears+"\t"+j+"\t"+i+"\tindividuals/month\n");
				}
			}
		}
		for (int i=0;i<demes.size();i++) {
			for (int j=0;j<demes.size();j++) {
				for (int k=0;k<12;k++) {
					seasStream.print("infections\t"+k+"\t"+demes.get(i).infectionsFrom[k][j]/numYears+"\t"+j+"\t"+i+"\tindividuals/month\n");
				}
			}
		}
		
		for (int i=0;i<demes.size();i++) {
			for (int j=0;j<demes.size();j++) {
				for (int k=0;k<12;k++) {
					seasStream.print("rate\t"+k+"\t"+demes.get(i).infectionRateFrom[k][j]/numYears+"\t"+j+"\t"+i+"\tmigrations/month\n");
				}
			}
		}
		
		for (int i=0;i<demes.size();i++) {
			for (int j=0;j<demes.size();j++) {
				for (int k=0;k<12;k++) {
					seasStream.print("transition\t"+k+"\t"+transitions[j][i][k]/numYears+"\t"+j+"\t"+i+"\tmigrations/month\n");
				}
			}
		}
			
		for (int i=0;i<demes.size();i++) {
			for (int j=0;j<12;j++) {
				seasStream.print("extant\t"+j+"\t"+extantLineages[i][j]/numYears+"\t"+i+"\tlineages\n");
			}
		}
		
		seasStream.close();
	}

}