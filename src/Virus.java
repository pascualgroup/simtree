/* Virus infection that has genotype, phenotype and ancestry */

import java.util.*;

public class Virus {

	// simulation fields
	private Virus parent;
	
	private Phenotype phenotype;

	private float birth;		// measured in years relative to burnin
	private byte deme;
	private float hostAge;		// age of host in years at time of infection

	// tree 
	// additional reconstruction fields
	private boolean marked;
	private boolean trunk;	// fill this at the end of the simulation
	private List<Virus> children = null; //new ArrayList<Virus>(0);	// will be void until simulation ends	
	private float layout;
	private int coverage;		// how many times this Virus has been covered in tracing the tree backwards
	
	
	
	// initialization
	public Virus() {
		phenotype = PhenotypeFactory.makeVirusPhenotype();
		birth = (float) Parameters.getDate();		
	}
	
	// replication, copies the virus, but remembers the ancestry
	public Virus(Virus v, byte d, float ha) {
		parent = v;		
		phenotype = v.getPhenotype();
		birth = Parameters.getDate();
		deme = d;
		hostAge = ha;
	}
	
	public Virus(Virus v, byte d, Phenotype p, float ha) {
		parent = v;
		phenotype = p;
		birth = Parameters.getDate();
		deme = d;
		hostAge = ha;
	}
	
	// replication, copies the virus, but remembers the ancestry
	public Virus(Virus v, byte d) {
		parent = v;
		phenotype = v.getPhenotype();
		birth = Parameters.getDate();
		deme = d;
	}
	
	public Virus(Virus v, byte d, Phenotype p) {
		parent = v;
		phenotype = p;
		birth = Parameters.getDate();
		deme = d;
	}	
	
	public Virus(Virus v) {
		this.birth = v.birth;
		this.children = new ArrayList<Virus>();
		this.children.addAll(v.children); 		
		this.coverage = v.coverage;
		this.deme = v.deme;
		this.hostAge = v.hostAge;
		this.layout = v.layout;
		this.marked = v.marked;
		this.phenotype = v.phenotype;
		this.trunk = v.trunk;				
	}
	
	// methods
	public Phenotype getPhenotype() {
		
		return phenotype;
	}
	public void setPhenotype(Phenotype p) {
		phenotype = p;
	}	
	public float getBirth() {
		return birth;
	}
	public Virus getParent() {
		return parent;
	}
	public void setParent(Virus v) {
		parent = v;
	}
	public double getHostAge() {
		return hostAge;
	}
	public boolean isTrunk() {
		return trunk; 
	}
	public void makeTrunk() {
		trunk = true;
	}
	public void mark() {
		marked = true;
	}
	public boolean isMarked() {
		return marked;
	}
	public int getDeme() {
		return deme;
	}	
	public double getLayout() {
		return layout;
	}
	public void setLayout(float y) {
		layout = y;
	}
	public int getCoverage() {
		return coverage;
	}
	public void incrementCoverage() {
		coverage++;
	}
	
	// add virus node as child if does not already exist
	public void addChild(Virus v) {
		if (children==null) {
			children = new ArrayList<Virus>(0);
		}
		if (!children.contains(v)) {
			children.add(v);
		}
	}		
	public int getNumberOfChildren() {
		if (children==null) {
			return 0;
		}
		return children.size();
	}
	public List<Virus> getChildren() {
		if (children==null) {
			children=new ArrayList<Virus>(0);
		}
		return children;
	}	
	public boolean isTip() {
		return getNumberOfChildren() == 0 ? true : false;
	}
	
	// returns a mutated copy, original virus left intact
	public Virus mutate() {
	
		Phenotype mutP = phenotype.mutate();			// mutated copy
		Virus mutV = new Virus(this,deme,mutP);
		return mutV;
		
	}
	
	public Virus commonAncestor(Virus virusB) {
				
		Virus lineageA = this;
		Virus lineageB = virusB;
		Virus commonAnc = null;
		Set<Virus> ancestry = new HashSet<Virus>();		
		while (true) {
			if (lineageA.getParent() != null) {		
				lineageA = lineageA.getParent();
				if (!ancestry.add(lineageA)) { 
					commonAnc = lineageA;
					break; 
				}
			}
			if (lineageB.getParent() != null) {
				lineageB = lineageB.getParent();
				if (!ancestry.add(lineageB)) { 
					commonAnc = lineageB;
					break; 
				}
			}
		}	
		
		return commonAnc;
		
	}
	
	public double distance(Virus virusB) {
		Virus ancestor = commonAncestor(virusB);
		double distA = getBirth() - ancestor.getBirth();
		double distB = virusB.getBirth() - ancestor.getBirth();
		return distA + distB;
	}
	
	public String toString() {
		return Integer.toHexString(this.hashCode());
	}

	public void collapse(Virus transformTo) {
		this.birth = transformTo.birth;
		this.children = transformTo.children;
		for (Virus child : children) {
			child.setParent(this);
		}
		this.coverage=transformTo.coverage;
		this.deme=transformTo.deme;
		this.hostAge=transformTo.hostAge;
		this.layout=transformTo.layout;
		this.marked=transformTo.marked;
		this.phenotype=transformTo.phenotype;
		this.trunk=transformTo.trunk;		
	}

	public void removeFirstChild() {
		children.remove(0);		
	}

	public void removeAllButFirstChild() {
		while (children.size()>1) {
			children.remove(children.size()-1);
		}		
	}

	public void fixParentOfChildren() {
		for (Virus child : children) {
			child.setParent(this);
		}
		
	}
	
	
	

}