import java.util.BitSet;

/* Acts as constructor for Phenotype objects */
/* A completely static class */

public class PhenotypeFactory {

	// returns newly instantiated Phenotype objects of type according to Parameters.phenotypeSpace
	public static Phenotype makeVirusPhenotype() {

		Phenotype p = null;
		switch (Parameters.phenotypeSpace) {
		case GEO : { p = new GeometricPhenotype(); break;}
		case EPOCH : { p = new PhenotypeEpochal();  break;}

		case LDSUNETRA : { p = new PhenotypeLimitedDiversitySunetra();  break;}
		case LDLINEAR : { p = new PhenotypeLimitedDiversityLinearXI();  break;}
		case LDLINEAROLD : { p = new PhenotypeLimitedDiversityLinearXIOLD();  break;}
		case GEO3D : { p = new GeometricPhenotype3D();  break;}
		default:
			System.err.println("Unable to resolve phenotype type!");			;
		}
		return p;

	}

	// returns newly instantiated Phenotype objects of type according to Parameters.phenotypeSpace
	public static Phenotype makeHostPhenotype() {

		Phenotype p = null;
		switch (Parameters.phenotypeSpace) {
		case GEO : { p = new GeometricPhenotype(GeometricPhenotype.initialTraitA, GeometricPhenotype.initialTraitB);  break;}	
		case EPOCH : {  p = new PhenotypeEpochal(PhenotypeEpochal.initialTraitA, PhenotypeEpochal.initialTraitB);  break;}

		case LDSUNETRA : {  p = new PhenotypeLimitedDiversitySunetra( PhenotypeLimitedDiversitySunetra.randomTraits() );  break;}
		case LDLINEAR :
			switch (PhenotypeLimitedDiversityLinearXI.initialMemory) {
			case Random:
				p = new PhenotypeLimitedDiversityLinearXI(PhenotypeLimitedDiversityLinearXI.randomTraits()); 
				break;
			case Fixed :
				p = new PhenotypeLimitedDiversityLinearXI( PhenotypeLimitedDiversityLinearXI.initialLimitedDiversityPhenotype ); 
				break;
			case Partial :
				p = new PhenotypeLimitedDiversityLinearXI( PhenotypeLimitedDiversityLinearXI.partialLimitedDiversityPhenotype ); 
				break;
			}
			break;

		case LDLINEAROLD : {p = new PhenotypeLimitedDiversityLinearXIOLD( PhenotypeLimitedDiversityLinearXIOLD.randomTraits() );  break; }
		case GEO3D : { p = new GeometricPhenotype3D(GeometricPhenotype3D.initialTraitA, GeometricPhenotype3D.initialTraitB, GeometricPhenotype3D.initialTraitC);  break; }
		default:
			System.err.println("Unable to resolve phenotype type!");			
		}
		return p;

	}	

	// returns newly instantiated Phenotype objects of type according to Parameters.phenotypeSpace
	public static Phenotype makeArbitaryPhenotype(double x, double y) {

		Phenotype p = null;

		switch (Parameters.phenotypeSpace) {
		case GEO : { p = new GeometricPhenotype(x, y); break;} 	
		case EPOCH : { p = new PhenotypeEpochal(x, y);  break;} 
		default:
			System.err.println("phonetype not implemented for this function!");
			System.exit(1);			

		}

		return p;

	}

	// returns newly instantiated Phenotype objects of type according to Parameters.phenotypeSpace
	public static Phenotype makeArbitaryPhenotype(int[] traits) {

		Phenotype p = null;
		switch (Parameters.phenotypeSpace) {		
		case LDLINEAROLD :{p = new PhenotypeLimitedDiversityLinearXIOLD(traits); break; }
		default:
			System.err.println("phonetype not implemented for this function!");
			System.exit(1);			

		}			 
		return p;	
	}

	// returns newly instantiated Phenotype objects of type according to Parameters.phenotypeSpace
	public static Phenotype makeArbitaryPhenotype(BitSet traits) {

		Phenotype p = null;
		switch (Parameters.phenotypeSpace) {
		case LDSUNETRA :{ p = new PhenotypeLimitedDiversitySunetra(traits);  break;}
		default:
			System.err.println("phonetype not implemented for this function!");
			System.exit(1);			



		}			 
		return p;	
	}


	// returns newly instantiated Phenotype objects of type according to Parameters.phenotypeSpace
	public static Phenotype makeArbitaryPhenotype(int traits) {

		Phenotype p = null;
		switch (Parameters.phenotypeSpace) {

		case LDLINEAR :{p = new PhenotypeLimitedDiversityLinearXI(traits); break; }
		default:
			System.err.println("phonetype not implemented for this function!");
			System.exit(1);			


		}			 
		return p;	
	}

}
