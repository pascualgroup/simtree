/* A population of host individuals */

import java.util.*;
import java.util.Map.Entry;
import java.io.*;

public class PhenotypePopulation {

	// fields
	HashMap<Long, Phenotype> trackedPhenotypes = null;
	// LD strain infected and risk log;
	//double riskArray[] = null;
	HashMap<Long, Double> riskArray = null;
	//int infectedArray[] = null;
	HashMap<Long, Integer> infectedArray = null;
	//int casesArray[] = null;
	HashMap<Long, Integer> casesArray = null;	
	//boolean firstTimePrinting[] = null;
	Set<Long> printedPhenotypeAlready = null;
	//boolean crossedSignificantInfectedThreshold[] = null;
	Set<Long> crossedSignificantInfectedThreshold = null;
	final double significantInfectedThresholdP = 0.0005;
	private int popSize = 0; 

	PhenotypePopulation(int popSize_) {
		reset(popSize_);
	}
	
	void reset(int popSize_) {
		
		trackedPhenotypes = new HashMap<Long, Phenotype>();
	
		popSize = popSize_;
		switch (Parameters.phenotypeSpace) {

		case LDSUNETRA : {
			// Generate list of all limited diversity phenotypes
			
			if (Parameters.printAllPhenotypes) {
				PhenotypeLimitedDiversitySunetra p = new PhenotypeLimitedDiversitySunetra(PhenotypeLimitedDiversitySunetra.initialLimitedDiversityPhenotype);
				do {				
					trackedPhenotypes.put(p.getIndex(),p);		
					p = new PhenotypeLimitedDiversitySunetra(p.getTraits());
				} while (p.increaseTraits());
			}
			
			break;
		}

		case LDLINEAR : {
			// Generate list of all limited diversity phenotypes
			
			if (Parameters.printAllPhenotypes) {
				PhenotypeLimitedDiversityLinearXI p = new PhenotypeLimitedDiversityLinearXI(PhenotypeLimitedDiversityLinearXI.initialLimitedDiversityPhenotype);

				do {									
					trackedPhenotypes.put(p.getIndex(),p);
					p = new PhenotypeLimitedDiversityLinearXI(p.getTraits());					
				} while (p.increaseTraits());
				p = new PhenotypeLimitedDiversityLinearXI(p.getTraits());
				trackedPhenotypes.put(p.getIndex(),p);	
			}
					
			//System.err.println("numPossibleStrains: "+ numPossibleStrains);
			break;
		}

		case LDLINEAROLD : {
			// Generate list of all limited diversity phenotypes
//			PhenotypeLimitedDiversityLinearXIOLD p = new PhenotypeLimitedDiversityLinearXIOLD(PhenotypeLimitedDiversityLinearXIOLD.initialLimitedDiversityPhenotype);
//			do {				
//				trackedPhenotypes.put(p.getIndex(),p);		
//				p = new PhenotypeLimitedDiversityLinearXIOLD(p.getTraits());
//			} while (p.increaseTraits());
//			
			break;
		}

		case GEO : {
			// Generate list of GEO phenotypes
			// this isn't implemented
//			trackedPhenotypes.put((long) 0, new GeometricPhenotype(GeometricPhenotype.initialTraitA,GeometricPhenotype.initialTraitB));
			break;
		}

		case GEO3D : {
			// Generate list of GEO3D phenotypes
			// this isn't implemented
//			trackedPhenotypes.put((long) 0, new GeometricPhenotype3D(GeometricPhenotype3D.initialTraitA,GeometricPhenotype3D.initialTraitB,GeometricPhenotype3D.initialTraitC));
			break;
		}

		case EPOCH : {
			// Generate list of EPOCH phenotypes
			// this isn't implemented
			//trackedPhenotypes.put((long) 0, new PhenotypeEpochal(PhenotypeEpochal.initialTraitA,PhenotypeEpochal.initialTraitB));
			break;
		}


		default: break;
		}	

		//riskArray = new double[numPossibleStrains+1];
		riskArray = new HashMap<Long,Double>();
		//infectedArray = new int[numPossibleStrains+1];
		infectedArray = new HashMap<Long,Integer>();
		//casesArray = new int[numPossibleStrains+1];
		casesArray = new HashMap<Long,Integer>();
		//crossedSignificantInfectedThreshold = new boolean[numPossibleStrains+1];
		crossedSignificantInfectedThreshold = new HashSet<Long>();
		//firstTimePrinting= new boolean[numPossibleStrains+1];
		printedPhenotypeAlready = new HashSet<Long>();
		
//		riskArray.clear();
//		infectedArray.clear();
//		casesArray.clear();
//		firstTimePrinting.clear();
//		crossedSignificantInfectedThreshold.clear();
//		trackedPhenotypes.clear();
		
	}

	void add(Phenotype p) {
		Long index = p.getIndex();		
		int infected = 0;
		int cases = 0;
		if (!trackedPhenotypes.containsKey(index)) {
			trackedPhenotypes.put(index, p);
		}
		if (infectedArray.containsKey(index)) 
			infected=infectedArray.get(index);					
		if (casesArray.containsKey(index)) 
			cases=casesArray.get(index);			
		infectedArray.put(index,infected+1);
		casesArray.put(index,cases+1);
		if ((infected+1)>significantInfectedThresholdP*((double) popSize) && Parameters.day>Parameters.burnin) {
			crossedSignificantInfectedThreshold.add(index);
		}	
	}

	void remove(Phenotype p) {
						
		Long index = p.getIndex();		
		int infected = 0;	
		if (infectedArray.containsKey(index)) 
			infected=infectedArray.get(index);			
		
		assert infected!=0;
		
		infectedArray.put(index,infected-1);		
	}

	boolean lastOfItsKind(Phenotype p) {
		Long index = p.getIndex();		
		int infected = 0;	
		if (infectedArray.containsKey(index)) 
			infected=infectedArray.get(index);			
		
		return infected<2;
	}

	public void printState(PrintStream printStream, int demeInfo) {

	//	for (Phenotype p : trackedPhenotypes.values()) {	
			
	//		Long index = p.getIndex();
		
		for (Entry<Long,Phenotype> entry :trackedPhenotypes.entrySet()) {
			Phenotype p = entry.getValue();
			long index = entry.getKey();
			int infected = 0;
			int cases = 0;
			double risk = 0.0;
			if (infectedArray.containsKey(index)) 
				infected=infectedArray.get(index);										
			if (casesArray.containsKey(index)) 
				cases=casesArray.get(index);			
			if (riskArray.containsKey(index)) 
				risk=riskArray.get(index);			
			
			if (printedPhenotypeAlready.contains(index)) {
				//System.out.printf("%.4f\t%d\t%.4f\t%d\t%d\t%d\t%s\n",Parameters.getDate(),p.getIndex(),riskArray[p.getIndex()],infectedArray[p.getIndex()],casesArray[p.getIndex()],demeInfo,p.toString("\t"));
				printStream.printf("%.4f\t%d\t%.4f\t%d\t%d\t%d\t\n",Parameters.getDate(),index,risk,infected,cases,demeInfo);
			}
			else {
				printedPhenotypeAlready.add(index);
				printStream.printf("%.4f\t%d\t%.4f\t%d\t%d\t%d\t%s\n",Parameters.getDate(),index,risk,infected,cases,demeInfo,p.toString("\t"));					
				//System.out.printf("%.4f\t%d\t%.4f\t%d\t%d\t%d\t\n",Parameters.getDate(),p.getIndex(),riskArray[p.getIndex()],infectedArray[p.getIndex()],casesArray[p.getIndex()],demeInfo);
			}

		};

	}

	public void clear() {
		riskArray.clear();
		infectedArray.clear();
		casesArray.clear();
		printedPhenotypeAlready.clear();
		crossedSignificantInfectedThreshold.clear();
		trackedPhenotypes.clear();
	}

	public int getInfected(Phenotype p) {
		long index = p.getIndex();
		
		if (infectedArray.containsKey(index)) {
			return infectedArray.get(index);
		}
		else {
			return 0;
		}		
	}


	//public Collection<Phenotype> getAllPossibleStrains() {
	//	return trackedPhenotypes.values();
	//}

	public double getRisk(Phenotype p) {
		long index = p.getIndex();
		
		if (riskArray.containsKey(index)) {
			return riskArray.get(index);
		}
		else {
			return 0;
		}		
	}

	public void increaseRisk(Phenotype p, double d) {
		long index = p.getIndex();
		
		if (riskArray.containsKey(index)) {
			riskArray.put(index,riskArray.get(index)+d);
		}
		else {
			riskArray.put(index,d);
		}		
	}

	public void resetRiskAndCases() {
//		for (int i=0;i<riskArray.length;i++) {
//			riskArray[i]=0;
//			casesArray[i]=0;
//		}
		for (Long key : riskArray.keySet()) {
			riskArray.put(key, 0.0);
		}
		for (Long key : casesArray.keySet()) {
			casesArray.put(key, 0);
		}
		
	}

	public boolean casesInThisPeriod(Phenotype p)  {
		
		Long index = p.getIndex();		
		Integer cases = null;
		if (casesArray.containsKey(index)) 
			cases=casesArray.get(index);			
		else 
			cases=0;
		
		return cases>0;
	}

	public int getNumSignificantStrains() {
		return crossedSignificantInfectedThreshold.size();
	}

	public Collection<Phenotype> getTrackedStrains() {		
		return trackedPhenotypes.values();
	}
	

}