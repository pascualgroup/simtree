/* Stores parameters for use across simulation */
/* Start with parameters in source, implement input file later */
/* A completely static class.  */

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.GregorianCalendar;

enum PhenotypeType {GEO, GEO3D, LD, LDLINEAR, EPOCH, LDLINEAROLD, LDSUNETRA};
enum BetweenDemeContactType {PROPORTIONAL, CONSTANT, TEST_STEP};

public class Parameters {

	@Setting static int param_index = -1;  // this sets a parameter based on an indexed list
	// ignored if param_index = -1 !!!

	//@Setting static boolean random_parmas = false;
	//@Setting static int param_index
	//@Setting static double[] gridX_settings = {0, 1, 11};     
	//@Setting static double[] gridY_settings = {-6, -1, 10};  
	//@Setting static String gridX_param = "muPhenotype";
	//@Setting static String gridY_param = "slope";

	@Setting static int gridpoints = 21;

	@Setting static boolean random_key_params = false; // this generates a random value for specific key parameters 
	// (in our case slope,gamma,mu_phenotye)				
	// simulation parameters
	public static int day = 0;		
	@Setting (	description ="Burn In time in days. Initial run time without recording output."	
			) static int burnin = 365*(1999-1968);  
	@Setting static int endDay = 365*(2014-1968); 
	@Setting static int printStepTimeseries = 30;					// print to out.timeseries every week
	@Setting static int printStepImmunity = 30;						// print to out.immunity every 2 month
	@Setting static double tipSamplingRate = 0.28;				    // in samples per deme per day
	@Setting static int[] tipSamplesPerDeme = {528/*56*/,150/*1*/,1302/*270*/};	
	@Setting static boolean tipSamplingProportional = true;	// whether to sample proportional to prevalance

	@Setting static double treeProportion = 0.01;					// proportion of tips to use in tree reconstruction
	@Setting static int	diversitySamplingCount = 10;
	@Setting static boolean repeatSim = true;						// repeat simulation until endDay is reached?
	@Setting static boolean immunityReconstruction = true;			// whether to print immunity reconstruction to out.immunity
	@Setting static boolean memoryProfiling = false;

	@Setting static double yearsFromMK = 5;							// subtract this many years off the end of the simulation when doing MK test	
 
	// metapopulation parameters	
	@Setting static String[] demeNames ={"D1","D2","D3"};  
	@Setting static int[] initialNs = {10000000,3000000,57000000};//{3333334,3333333,3333333}; //25257_7
	
	// host parameters
	@Setting static double birthRate = 1.0/(30.0*365.0);		// in births per individual per day, 1/30 years = 0.000091
	@Setting static double deathRate = 1.0/(30.0*365.0);		// in deaths per individual per day, 1/30 years = 0.000091
	@Setting static boolean swapDemography = true;				// whether to keep overall population size constant


	// epidemiological parameters
	@Setting static int initialI = 1;//10666					// in individuals
	@Setting static double initialPrR = 3; 						// proportion with previously encountered random (or fixed) phenotype (can be more than 1)
	@Setting static double beta = 0.6; // 0.6542				// in contacts per individual per day
	@Setting static double nu = 0.2; //0.1518					// in recoveries per individual per day // was 18
	
	@Setting static BetweenDemeContactType betweenDemeContactType = BetweenDemeContactType.PROPORTIONAL;
	@Setting static double[][] betweenDemePro = {{0.000,0.031,0.024},
												 {0.032,0.000,0.038},
												 {0.042,0.010,0.000}};	
	
	@Setting static double[][] constantContactRate = {{0 ,10,10},
													  {10,0 , 0},
													  {10,0 , 0}};
	
	// transcendental immunity
	// @Setting static double immunityLoss = 0;					// in R->S per individual per day
	
	// @Setting static maintenanceStop = 10000;  

	// phenotype parameters	//6 was prev
	@Setting static PhenotypeType phenotypeSpace = PhenotypeType.LDLINEAR;		// options include: "geometric", "geometric3d", "ld", "linearld"
	@Setting static double muPhenotype = 8E-6; //0.0002		// in mutations per individual per day // was 8E-6

	// Keep alive during burn-in...w
	@Setting static boolean keepAliveDuringBurnin = true;

	@Setting ( description ="Allows for strain mutatation only during burn-in." )  
	static boolean mutateOnlyDuringBurnin =false;


	// Keep alive after and during burn-in
	@Setting static boolean keepAlive = true;

	// Once a mutation introduces a phenotype to a deme it can't die in this deme...
	@Setting public static boolean keepStrainsAlive = false;			

	public static Virus urVirus = null;

	@Setting static double percentHostsForImmunityReconstruction = 0.0002;
	@Setting public static boolean printAllPhenotypes = true;

	static final int defaultNumEpitopes = 20;

	// measured in years, starting at burnin
	public static float getDate() {
		return (float) (((float) day - (float) burnin ) / 365.0);
	}

	// seasonal betas
	@Setting static double[] demeBaselines =  {1.000,   1.0,  	1.0  };
	@Setting static double[] demeAmplitudes1 ={0.060,   0.407,  0.197}; 
	@Setting static double[] demeOffsets1 =   {0.711,   0.961,  0.063};
	@Setting static double[] demeAmplitudes2 ={0.192,   0.019,  0.141};
	@Setting static double[] demeOffsets2 =   {0.120,   0.224,  0.109};			// relative to 1/2 year
	@Setting static double[] noise ={0.0, 0.0,0.0};//,0}; 	

	@Setting static boolean silent = false;
	
	// migration matrix (this is multiplied by between deme beta)
												//CH 	EU 	JA	OC 	SA 	SEA US
	//@Setting static double[] imigrationVector = {0.79, 0.7, 0.76, 1.27, 0.47, 0.85, 0.7};
	//@Setting static double[] emigrationVector = {1.05, 0.59, 0.51,	0.55, 0.52,	0.91, 1.62};

	static Settings s;

	
	
	public static double getSeasonality(int index) {
		double lastNoise[] = new double[noise.length];
		double baseline = demeBaselines[index];
		double amplitude1 = demeAmplitudes1[index];
		double offset1 = demeOffsets1[index];
		double amplitude2 = demeAmplitudes2[index];
		double offset2 = demeOffsets2[index];

		lastNoise[index] = lastNoise[index]+Random.nextDouble(-1,1)*noise[index];	

		return baseline + amplitude1 * Math.cos(2*Math.PI*getDate() - 2*Math.PI*offset1) +
				+ amplitude2 * Math.cos(4*Math.PI*getDate() - 4*Math.PI*offset2)+lastNoise[index];
	}

	public static void init() {

		day=0;
		urVirus = new Virus();

		if (param_index>=0) {

			double[] mu_range= new double[Parameters.gridpoints];
			for (int i=0; i<Parameters.gridpoints; i++) {
				mu_range[i]=Math.pow(10.0, -5.6+2.8/((double)Parameters.gridpoints-1.0)*((double) i));
			}

			muPhenotype = mu_range[Parameters.param_index/mu_range.length];	
		}

		if (random_key_params) {
			muPhenotype = Math.pow(10,Random.nextDouble(-5.6, -2.8));
		}
		
		switch (Parameters.phenotypeSpace) {
		case GEO : { 
			s.apply(GeometricPhenotype.class);
			s.apply(ImmuneSystemPhenotypeHistoryBased.class);
			break;}
		case EPOCH : { 
			s.apply(PhenotypeEpochal.class); 
			s.apply(ImmuneSystemPhenotypeHistoryBased.class);
			break;}
	
		case LDSUNETRA : { 
			PhenotypeLimitedDiversitySunetra.init();
			s.apply(ImmuneSystemEpitopeNPhenotypeHistoryBased.class);
			s.apply(PhenotypeLimitedDiversitySunetra.class);			
			//			s.applyRandom(PhenotypeLimitedDiversity.class, Parameters.yParamName, Parameters.yRange);
			break;
		}
		case LDLINEAR : { 
			s.apply(ImmuneSystemEpitopeHistoryBased.class);
			PhenotypeLimitedDiversityLinearXI.init();
			s.apply(PhenotypeLimitedDiversityLinearXI.class);			
			break;
		}
		case LDLINEAROLD : { 
			s.apply(ImmuneSystemPhenotypeHistoryBased.class);
			PhenotypeLimitedDiversityLinearXIOLD.init();
			s.apply(PhenotypeLimitedDiversityLinearXIOLD.class);			
			break;
		}
		case GEO3D : { 
			s.apply(GeometricPhenotype3D.class);
			s.apply(ImmuneSystemPhenotypeHistoryBased.class);
			break;
		}		
		default:
			System.err.println("phonetype not implemented for this function!");
			System.exit(1);			

		}		

	}

	public static void applyArgsAndInit(String[] args) {
		s = new Settings(args);
		s.apply(Parameters.class);
		init();
		
	}
	
	public static void printParams() {

		try {
			File paramFile = new File("out.params");
			paramFile.delete();
			paramFile.createNewFile();
			PrintStream paramStream = new PrintStream(paramFile);
			Settings.printSettings(Parameters.class, paramStream);

			switch (Parameters.phenotypeSpace) {
			case GEO : { Settings.printSettings(GeometricPhenotype.class, paramStream); Settings.printSettings(ImmuneSystemPhenotypeHistoryBased.class, paramStream); break;}
			case EPOCH : {Settings.printSettings(PhenotypeEpochal.class, paramStream); Settings.printSettings(ImmuneSystemPhenotypeHistoryBased.class, paramStream); break;}
			case LDSUNETRA : { Settings.printSettings(PhenotypeLimitedDiversitySunetra.class, paramStream); Settings.printSettings(ImmuneSystemEpitopeNPhenotypeHistoryBased.class, paramStream); break;}
			case LDLINEAR : {Settings.printSettings(PhenotypeLimitedDiversityLinearXI.class, paramStream); Settings.printSettings(ImmuneSystemEpitopeHistoryBased.class, paramStream); break;}
			case LDLINEAROLD : {Settings.printSettings(PhenotypeLimitedDiversityLinearXIOLD.class, paramStream); Settings.printSettings(ImmuneSystemPhenotypeHistoryBased.class, paramStream); break;}
			case GEO3D : { Settings.printSettings(GeometricPhenotype3D.class, paramStream); Settings.printSettings(ImmuneSystemPhenotypeHistoryBased.class, paramStream); break;}
			default:
				System.err.println("phonetype not implemented for this function!");
				System.exit(1);			

			}		
				
			paramStream.print((new GregorianCalendar()).getTime());
												
			paramStream.close();
		} catch(IOException ex) {
			System.out.println("Could not write to file out.params!"); 
			System.exit(0);
		}
	}

}
