


public class Host {

	// fields
	private Virus infection;												
	private ImmuneSystem immuneSystem = ImmuneSystemFactory.makeImmuneSystem();
	private float birth;	// measured in years relative to burnin	

		
	// naive host
	public Host() {
		float lifespan = (float) (1 / (365.0 * Parameters.birthRate));
		float age = (float) Random.nextExponential(lifespan);
		birth = (float) (Parameters.getDate() - age);	
	}
	
	// initial infected host
	public Host(Virus v) {
		float lifespan = (float) (1 / (365.0 * Parameters.birthRate));
		float age = (float) Random.nextExponential(lifespan);
		birth = (float) (Parameters.getDate() - age);
		infection = v;
		
	}
	
	
	// infection methods
	public void reset() {
		birth = (float) Parameters.getDate();
		infection = null;
		immuneSystem.reset();
	}
	
	public float getBirth() {
		return birth;
	}
	
	public boolean isInfected() {
		boolean infected = false;
		if (infection != null) {
			infected = true;
		}
		return infected;
	}
	public Virus getInfection() {
		return infection;
	}
	public void infect(Virus pV, byte d) {
		float hostAge = Parameters.getDate() - birth;
		Virus nV = new Virus(pV, d, hostAge);
		infection = nV;
	}
	
	public void clearInfection() {
		Phenotype p = infection.getPhenotype(); 	
		immuneSystem.add(p);	
		infection = null;		
	}

	
	// make a new virus with the mutated phenotype
	public void mutate() {
		Virus mutV = infection.mutate();
		infection = mutV;
	}
	
	public double riskOfInfection( Phenotype p) {
		return immuneSystem.riskOfInfection(p);
	}
	
	public void addToImmuneHistory( Phenotype p) {
		immuneSystem.add(p);
	}

	public String printImmuneState() {	
		return immuneSystem.printState();		
	}
	
//	public void infect() {
//		infected = true;
//	}
//	
//	public void uninfect() {
//		infected = false;
//	}
//	
	

	
	


}