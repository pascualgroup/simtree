

public class ImmuneSystemEpitopeHistoryBased implements ImmuneSystem{
	
	
	
	 
	
	int immuneHistory = 0;
	int lastInfectionTime = Integer.MIN_VALUE; // [days]
	
	//@Setting static float fractionMemory = 1; // Fraction of epitopes remembered (epitope based immune history only)	
	@Setting static boolean shortTermImmunity = false; // Immunity to all strains for a specific period following recovery
	@Setting static int shortTermImmunityLength = 90; // [days]
	
	//static BitSet noImmuneHistory = new BitSet(Parameters.defaultNumEpitopes);

	
	ImmuneSystemEpitopeHistoryBased() {
		immuneHistory=0;
		
	}
	
	public void reset() {	
		immuneHistory=0;	
		
		assert BitManip.cardinality(immuneHistory)==0 || BitManip.cardinality(immuneHistory)>=PhenotypeLimitedDiversityLinearXI.nLoci; 
				
//		lastInfectionTime = Integer.MIN_VALUE;
	}
	
	@Override
	public double riskOfInfection(Phenotype p) {
		if (shortTermImmunity==true) {
			if (Parameters.day>(lastInfectionTime+shortTermImmunityLength)) {
				return p.riskOfInfection(this);
			}
			else 
				return 0;
		}
		else {
			return p.riskOfInfection(this);			
		}
	}

	@Override
	public String printState() {
		
		String state = new String();
		state = state + "Allocated Length: 1 integer\n";		
				
		return state;
	}

	public int getHistory() {
		
	
		assert BitManip.cardinality(immuneHistory)==0 || BitManip.cardinality(immuneHistory)>=PhenotypeLimitedDiversityLinearXI.nLoci;
		
		return immuneHistory;
	}
	
	
	public void addEpitopes( long traits ) {	
	
		assert BitManip.cardinality(immuneHistory)==0 || BitManip.cardinality(immuneHistory)>=PhenotypeLimitedDiversityLinearXI.nLoci;
//		if (fractionMemory==1) {
		immuneHistory|=traits;
			
		assert BitManip.cardinality(immuneHistory)==0 || BitManip.cardinality(immuneHistory)>=PhenotypeLimitedDiversityLinearXI.nLoci;
//		}
//		else {			
//			BitSet rememberedEpitopes = new BitSet(Parameters.defaultNumEpitopes);
//			for (int i=0; i<traits.length(); i++) {
//				if (traits.get(i)==true) {
//					if (Random.nextBoolean(fractionMemory)) {
//						rememberedEpitopes.set(i);
//					}
//				}			
//			}
//			immuneHistory.or(rememberedEpitopes);
//		}
			
	}

	@Override
	public void add(Phenotype p) {
		if (shortTermImmunity==true) {
			lastInfectionTime=Parameters.day;
		}
		p.add(this);		
	}

	


	
	
	
	
}
