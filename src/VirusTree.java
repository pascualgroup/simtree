/* Stores a list of Viruses that have sampled during the course of the simulation */

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

public class VirusTree {

	// fields
	private static Virus root = Parameters.urVirus;	
	private static List<Virus> tips = new ArrayList<Virus>();
	private static List<Virus> tipsForNexus = new ArrayList<Virus>();
	
	public static double xMin;
	public static double xMax;
	public static double yMin;
	public static double yMax;
	public static double zMin;
	public static double zMax;	

	static final Comparator<Virus> descendantOrder = new Comparator<Virus>() {
		public int compare(Virus v1, Virus v2) {
			Integer descendantsV1 = new Integer(getNumberOfDescendants(v1));
			Integer descendantsV2 = new Integer(getNumberOfDescendants(v2));
			return descendantsV1.compareTo(descendantsV2);
		}
	};	

	// static methods
	public static void add(Virus v) {		
		tips.add(v);
	}
	public static void clear() {
		tips.clear();
	}
	public static List<Virus> getTips() {
		return tips;
	}
	public static Virus getRoot() {
		return root;
	}

	// return a random tip that lies between year from and year to
	public static Virus getRandomTipFromTo(float from, float to) {

		// fill temporary list
		List<Virus> select = new ArrayList<Virus>();
		for (Virus v : tips) {
			float x = v.getBirth();
			if (x >= from && x < to) {
				select.add(v);
			}
		}

		// pull random virus from this list
		Virus rV = null;
		if (select.size() > 0) {	
			int index = Random.nextInt(0,select.size()-1);
			rV = select.get(index);
		}
		return rV;

	}

	public static int getDemeCount(int d) {
		int count = 0;
		for (Virus v : tips) {
			if (v.getDeme() == d) {
				count++;
			}
		}
		return count;
	}	

	// work backwards for each sample filling the children lists
	public static void fillBackward() {

		for (Virus child : tips) {
			Virus parent = child.getParent();
			while (parent != null) {
				parent.addChild(child);
				parent.incrementCoverage();
				child = parent;
				parent = child.getParent();
			}
		}

	}

	public static void dropTips() {

		List<Virus> reducedTips = new ArrayList<Virus>();
		for (Virus v : tips) {
			if (Random.nextBoolean(Parameters.treeProportion)) {
				reducedTips.add(v);
			}
		}
		tips = reducedTips;

	}

	// marking to by time, not proportional to prevalence
	public static void markTips() {

		//		for (Virus v : tips) {
		//			if (Random.nextBoolean(Parameters.treeProportion)) {
		//				while (v.getParent() != null) {
		//					v.mark();
		//					v = v.getParent();
		//				}
		//			}
		//		}

		for (float i = 0; i < Parameters.getDate(); i+=0.1) {
			Virus v = getRandomTipFromTo(i,i+(float)0.1);
			if (v != null) {
				while (v.getParent() != null) {
					v.mark();
					v = v.getParent();
				}
			}
		}

	}

	// prune tips
	public static void pruneTips() {

		List<Virus> reducedTips = new ArrayList<Virus>();
		for (int d = 0; d < Parameters.initialNs.length; d++) {
			double keepProportion = (double) Parameters.tipSamplesPerDeme[d] / (double) getDemeCount(d);
			for (Virus v : tips) {
				if (Random.nextBoolean(keepProportion) && v.getDeme() == d) {
					reducedTips.add(v);
				}
			}
		}
		tips = reducedTips;

	}

	// returns virus v and all its descendents via a depth-first traversal
	public static List<Virus> postOrderNodes(Virus r) {

		List<Virus> descendantsAndRoot = new ArrayList<Virus>();
		descendantsAndRoot.add(r);

		Stack<Virus> S = new Stack<Virus>();	
		Virus u;
		S.push(r);
		while (!S.isEmpty()) {
			u = S.pop();
			for (Virus v : u.getChildren()) {
				descendantsAndRoot.add(v);
				S.push(v);                
			}
		}    
		return descendantsAndRoot;


	}

	// Count total descendents of a Virus, working through its children and its children's children
	public static int getNumberOfDescendants(Virus r) {

		int numberOfDescendants = 0;

		Stack<Virus> S = new Stack<Virus>();
		Virus u;

		S.push(r);
		while (!S.isEmpty()) {
			u = S.pop();
			for (Virus v : u.getChildren()) {
				numberOfDescendants+=1;
				S.push(v);                
			}
		}        

		return numberOfDescendants;
	}

	public static int getNumberOfDescendants() {
		return getNumberOfDescendants(root);
	}

	// sorts children lists so that first member is child with more descendents than second member
	public static void sortChildrenByDescendants(Virus r) {
		
		Collections.sort(r.getChildren(), descendantOrder);

		Stack<Virus> S = new Stack<Virus>();
		Virus u;
				
		S.push(r);
		while (!S.isEmpty()) {
			u = S.pop();
			for (Virus v : u.getChildren()) {
				Collections.sort(v.getChildren(), descendantOrder);
				S.push(v);                
			}
		}        
	}	

	public static void sortChildrenByDescendants() {
		sortChildrenByDescendants(root);
	}

	// sets Virus layout based on a postorder traversal
	public static void setLayoutByDescendants() {

		List<Virus> vNodes = postOrderNodes(root);

		// set layout of tips based on traversal
		float y = 0;
		for (Virus v : vNodes) {
			//			if (tips.contains(v)) {
			if (v.isTip()) {
				v.setLayout(y);
				y++;
			}
		}

		// update layout of internal nodes
		Collections.reverse(vNodes);
		for (Virus v : vNodes) {
			if (v.getNumberOfChildren() > 0) {
				float mean = 0;
				for (Virus child : v.getChildren()) {
					mean += child.getLayout();
				}
				mean /= v.getNumberOfChildren();
				v.setLayout(mean);
			}
		}

	}	

	// looks at a virus and its grandparent, if traits are identical and there is no branching
	// then make virus child rather than grandchild
	// returns v.parent after all is said and done
	public static Virus collapse(Virus v) {

		Virus vp = null;
		Virus vgp = null;
		if (v.getParent() != null) {
			vp = v.getParent();
			if (vp.getParent() != null) {
				vgp = vp.getParent();
			}
		}

		if (vp != null && vgp != null) {
			if (vp.getNumberOfChildren() == 1 && v.getPhenotype().toString().equals(vp.getPhenotype().toString()) && v.isTrunk() == vp.isTrunk() && v.getDeme() == vp.getDeme()) {

				List<Virus> vgpChildren = vgp.getChildren();
				int vpIndex =  vgpChildren.indexOf(vp);

				if (vpIndex >= 0) {

					// replace virus as child of grandparent
					vgpChildren.set(vpIndex, v);

					// replace grandparent as parent of virus
					v.setParent(vgp);

					// erase parent
					vp = null;

				}

			}
		}

		return v.getParent();

	}

	// walks backward using the list of tips, collapsing where possible
	public static void streamline() {

		for (Virus v : tips) {
			Virus vp = v;
			while (vp != null) {
				vp = collapse(vp);
			}
		}

	}

	// rotate the 2d euclidean space using PCA, returning an x-axis with maximum variance
	public static void rotate() {

		if (Parameters.phenotypeSpace == PhenotypeType.GEO) {

			// load a 2d array with phenotypes

			List<Virus> virusList = postOrderNodes(root);
			int n = virusList.size();
			int m = 2;

			double[][] input = new double[n][m];

			for (int i = 0; i < n; i++) {
				Virus v = virusList.get(i);
				GeometricPhenotype p = (GeometricPhenotype) v.getPhenotype();
				double x = p.getTraitA();
				double y = p.getTraitB();	
				input[i][0] = x;
				input[i][1] = y;				
			}

			// project this array

			double[][] projected = SimplePCA.project(input);

			// reset phenotypes based on projection

			for (int i = 0; i < n; i++) {
				Virus v = virusList.get(i);
				GeometricPhenotype p = (GeometricPhenotype) v.getPhenotype();
				double x = projected[i][0];
				double y = projected[i][1];				
				p.setTraitA(x);
				p.setTraitB(y);					
			}

		}	

		if (Parameters.phenotypeSpace == PhenotypeType.GEO3D) {

			// load a 2d array with phenotypes

			List<Virus> virusList = postOrderNodes(root);
			int n = virusList.size();
			int m = 3;

			double[][] input = new double[n][m];

			for (int i = 0; i < n; i++) {
				Virus v = virusList.get(i);
				GeometricPhenotype3D p = (GeometricPhenotype3D) v.getPhenotype();
				double x = p.getTraitA();
				double y = p.getTraitB();	
				double z = p.getTraitC();	
				input[i][0] = x;
				input[i][1] = y;		
				input[i][2] = z;
			}

			// project this array

			double[][] projected = SimplePCA.project3D(input);

			// reset phenotypes based on projection

			for (int i = 0; i < n; i++) {
				Virus v = virusList.get(i);
				GeometricPhenotype3D p = (GeometricPhenotype3D) v.getPhenotype();
				double x = projected[i][0];
				double y = projected[i][1];	
				double z = projected[i][2];	
				p.setTraitA(x);
				p.setTraitB(y);	
				p.setTraitC(z);	
			}

		}			

	}

	// flips the 2d euclidean space so that first sample is always to the left of the last sample
	public static void flip() {

		if (Parameters.phenotypeSpace == PhenotypeType.GEO) {

			List<Virus> virusList = postOrderNodes(root);
			int n = virusList.size();	

			// find first and last virus			
			Virus firstVirus = virusList.get(0);
			Virus lastVirus = virusList.get(0);
			double firstDate = firstVirus.getBirth();
			double lastDate = lastVirus.getBirth();

			for (Virus v : virusList) {
				if (v.getBirth() < firstDate) {
					firstDate = v.getBirth();
					firstVirus = v;
				}
				if (v.getBirth() > lastDate) {
					lastDate = v.getBirth();
					lastVirus = v;
				}				
			}

			// is the x-value of first virus greater than the x-value of last virus?
			// if so, flip

			GeometricPhenotype p = (GeometricPhenotype) firstVirus.getPhenotype();
			double firstX = p.getTraitA();
			p = (GeometricPhenotype) lastVirus.getPhenotype();
			double lastX = p.getTraitA();		

			if (firstX > lastX) {

				// I think that postOrderNodes() has replicates in it, need to go through some hoops because of this
				double[] input = new double[n];

				for (int i = 0; i < n; i++) {
					Virus v = virusList.get(i);
					p = (GeometricPhenotype) v.getPhenotype();		
					input[i] = p.getTraitA();;
				}

				for (int i = 0; i < n; i++) {
					Virus v = virusList.get(i);
					p = (GeometricPhenotype) v.getPhenotype();
					double x = -1*input[i];		
					p.setTraitA(x);
				}				

			}

		}

		if (Parameters.phenotypeSpace ==PhenotypeType.GEO3D) {

			List<Virus> virusList = postOrderNodes(root);
			int n = virusList.size();	

			// find first and last virus			
			Virus firstVirus = virusList.get(0);
			Virus lastVirus = virusList.get(0);
			double firstDate = firstVirus.getBirth();
			double lastDate = lastVirus.getBirth();

			for (Virus v : virusList) {
				if (v.getBirth() < firstDate) {
					firstDate = v.getBirth();
					firstVirus = v;
				}
				if (v.getBirth() > lastDate) {
					lastDate = v.getBirth();
					lastVirus = v;
				}				
			}

			// is the x-value of first virus greater than the x-value of last virus?
			// if so, flip

			GeometricPhenotype3D p = (GeometricPhenotype3D) firstVirus.getPhenotype();
			double firstX = p.getTraitA();
			p = (GeometricPhenotype3D) lastVirus.getPhenotype();
			double lastX = p.getTraitA();		

			if (firstX > lastX) {

				// I think that postOrderNodes() has replicates in it, need to go through some hoops because of this
				double[] input = new double[n];

				for (int i = 0; i < n; i++) {
					Virus v = virusList.get(i);
					p = (GeometricPhenotype3D) v.getPhenotype();		
					input[i] = p.getTraitA();;
				}

				for (int i = 0; i < n; i++) {
					Virus v = virusList.get(i);
					p = (GeometricPhenotype3D) v.getPhenotype();
					double x = -1*input[i];		
					p.setTraitA(x);
				}				

			}

		}		

	}

	// walks through list of nodes and update min and max ranges appropriately
	public static void updateRange() {

		xMin = 0.0;
		xMax = 0.0;
		yMin = 0.0;
		yMax = 0.0;
		zMin = 0.0;
		zMax = 0.0;		

		if (Parameters.phenotypeSpace == PhenotypeType.GEO) {
			for (Virus v : postOrderNodes(root)) {

				GeometricPhenotype p = (GeometricPhenotype) v.getPhenotype();
				double x = p.getTraitA();
				double y = p.getTraitB();
				if (xMin > x) { xMin = x; }
				if (xMax < x) { xMax = x; }
				if (yMin > y) { yMin = y; }
				if (yMax < y) { yMax = y; }	

			}
		}

		if (Parameters.phenotypeSpace == PhenotypeType.GEO3D) {
			for (Virus v : postOrderNodes(root)) {

				GeometricPhenotype3D p = (GeometricPhenotype3D) v.getPhenotype();
				double x = p.getTraitA();
				double y = p.getTraitB();
				double z = p.getTraitC();				
				if (xMin > x) { xMin = x; }
				if (xMax < x) { xMax = x; }
				if (yMin > y) { yMin = y; }
				if (yMax < y) { yMax = y; }	
				if (zMin > z) { zMin = z; }
				if (zMax < z) { zMax = z; }					

			}
		}		

		xMin = Math.floor(xMin) - 5;
		xMax = Math.ceil(xMax) + 5;
		yMin = Math.floor(yMin) - 5;
		yMax = Math.ceil(yMax) + 5;
		zMin = Math.floor(zMin) - 5;
		zMax = Math.ceil(zMax) + 5;		

	}

	public static void printRange() {

		try {
			File rangeFile = new File("out.range");
			rangeFile.delete();
			rangeFile.createNewFile();
			PrintStream rangeStream = new PrintStream(rangeFile);
			rangeStream.printf("%.4f,%.4f,%.4f,%.4f,%.4f,%.4f\n", xMin, xMax, yMin, yMax, zMin, zMax);
			rangeStream.close();
		} catch(IOException ex) {
			System.out.println("Could not write to file"); 
			System.exit(0);
		}

	}

	public static void printTips() {

		try {
			File tipFile = new File("out.tips");
			tipFile.delete();
			tipFile.createNewFile();
			PrintStream tipStream = new PrintStream(tipFile);
			tipStream.printf("\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s", "name", "year", "trunk", "tip", "mark", "location", "hostAge", "layout");					
			for (int i=0;i<VirusTree.getNumParts(); i++) {
				tipStream.printf(",\"ag%d\"",i+1);
			}
			tipStream.printf("\n");
			for (int i = 0; i < tips.size(); i++) {
				Virus v = tips.get(i);			
				tipStream.printf("\"%s\",%.4f,%d,%d,%d,%d,%.4f,%.4f,%s\n", v, v.getBirth(), v.isTrunk()?1:0, v.isTip()?1:0, v.isMarked()?1:0, v.getDeme(), v.getHostAge(), v.getLayout(), v.getPhenotype());
			}
			tipStream.close();
		} catch(IOException ex) {
			System.out.println("Could not write to file"); 
			System.exit(0);
		}

	}

	public static void printBranches() {

		try {
			File branchFile = new File("out.branches");
			branchFile.delete();
			branchFile.createNewFile();
			PrintStream branchStream = new PrintStream(branchFile);			
			for (Virus v : postOrderNodes(root)) {
				if (v.getParent() != null) {
					Virus vp = v.getParent();
					branchStream.printf("{\"%s\",%.3f,%d,%d,%d,%d,%.4f,%.3f,%s}\t", v, v.getBirth(), v.isTrunk()?1:0, v.isTip()?1:0, v.isMarked()?1:0, v.getDeme(), v.getHostAge(), v.getLayout(), v.getPhenotype());
					branchStream.printf("{\"%s\",%.3f,%d,%d,%d,%d,%.4f,%.3f,%s}\t", vp, vp.getBirth(), vp.isTrunk()?1:0, vp.isTip()?1:0, v.isMarked()?1:0, vp.getDeme(), v.getHostAge(), vp.getLayout(), vp.getPhenotype());
					branchStream.printf("%d\n", vp.getCoverage());
				}
			}
			branchStream.close();					
			
		} catch(IOException ex) {
			System.out.println("Could not write to file"); 
			System.exit(0);
		}

	}

	
	public static int getNumParts() {
		return root.getPhenotype().numParts();		
	}
	public static int sideBranchMutations() {
		int count = 0;
		for (Virus v : postOrderNodes(root)) {
			if (v.getParent() != null && v.getBirth() < (Parameters.getDate() - Parameters.yearsFromMK) && (v.getBirth()> 0) ) {
				Virus vp = v.getParent();
				if (!v.isTrunk() && !vp.isTrunk() &&!(v.getPhenotype().toString().equals(vp.getPhenotype().toString()))) {
					count++;
				}
			}
		}
		return count;
	}	

	public static int sideBranchMutationsPart(int n) {
		int count = 0;
		for (Virus v : postOrderNodes(root)) {
			if (v.getParent() != null && v.getBirth() < (Parameters.getDate() - Parameters.yearsFromMK) && (v.getBirth()> 0)) {
				Virus vp = v.getParent();
				if (!v.isTrunk() && !vp.isTrunk() &&!(v.getPhenotype().getPhenotypePart(n)==vp.getPhenotype().getPhenotypePart(n))) {
					count++;
				}
			}
		}
		return count;
	}	
	
	public static double sideBranchOpportunity() {
		double time = 0;
		for (Virus v : postOrderNodes(root)) {
			if (v.getParent() != null && v.getBirth() < (Parameters.getDate() - Parameters.yearsFromMK)&&(v.getBirth()>0)) {
				Virus vp = v.getParent();
				if (!v.isTrunk() && !vp.isTrunk()) {
					time += v.getBirth() - vp.getBirth();
				}
			}
		}
		return time;
	}	

	public static int trunkMutations() {
		int count = 0;
		for (Virus v : postOrderNodes(root)) {
			if (v.getParent() != null && v.getBirth() < (Parameters.getDate() - Parameters.yearsFromMK)&& (v.getBirth()> 0) ) {
				Virus vp = v.getParent();
				if (v.isTrunk() && vp.isTrunk() && !(v.getPhenotype().toString().equals(vp.getPhenotype().toString()))) {
					count++;
				}
			}
		}
		return count;
	}	
	
	
	public static int trunkMutationsPart(int n) {
		int count = 0;
		for (Virus v : postOrderNodes(root)) {
			if (v.getParent() != null && v.getBirth() < (Parameters.getDate() - Parameters.yearsFromMK)&& (v.getBirth()>0)) {
				Virus vp = v.getParent();
				if (v.isTrunk() && vp.isTrunk() && !(v.getPhenotype().getPhenotypePart(n)==vp.getPhenotype().getPhenotypePart(n))) {
					count++;
				}
			}
		}
		return count;
	}	

	public static double trunkOpportunity() {
		double time = 0;
		for (Virus v : postOrderNodes(root)) {
			if (v.getParent() != null && v.getBirth() < (Parameters.getDate() - Parameters.yearsFromMK)&&(v.getBirth()>0) ) {
				Virus vp = v.getParent();
				if (v.isTrunk() && vp.isTrunk()) {
					time += (v.getBirth() - vp.getBirth());
				}
			}
		}
		return time;
	}		

	public static void printMK() {

		try {
			File mkFile = new File("out.mk");
			mkFile.delete();
			mkFile.createNewFile();
			PrintStream mkStream = new PrintStream(mkFile);
			mkStream.printf("sideBranchMut,sideBranchOpp,sideBranchRate,trunkMut,trunkOpp,trunkRate,mk");
			for (int i=0; i<getNumParts(); i++) {
				mkStream.printf(",part"+i);
			}
			mkStream.printf("\n");
			int sideBranchMut = sideBranchMutations();
			double sideBranchOpp = sideBranchOpportunity();
			double sideBranchRate = sideBranchMut / sideBranchOpp;
			int trunkMut = trunkMutations();
			double trunkOpp = trunkOpportunity();	
			double trunkRate = trunkMut / trunkOpp;		
			double mk = trunkRate / sideBranchRate;
			mkStream.printf("%d,%.4f,%.4f,%d,%.4f,%.4f,%.4f", sideBranchMut, sideBranchOpp, sideBranchRate, trunkMut, trunkOpp, trunkRate, mk);
			
			for (int i=0; i<getNumParts(); i++) {
				mkStream.printf(","+ (trunkMutationsPart(i)/trunkOpportunity())/(sideBranchMutationsPart(i)/sideBranchOpportunity()));
			}
			mkStream.printf("\n");
			
			mkStream.close();
		} catch(IOException ex) {
			System.out.println("Could not write to file"); 
			System.exit(0);
		}

	}
	public static void init() {
		// TODO Auto-generated method stub
		root = Parameters.urVirus;	
		tips = new ArrayList<Virus>();		
	}	
	
	
	public static String newick() {
		return newick(root) + ";\n";
	}

	private static String newick(Virus treePart) {	
		String returnValue = new String();

		if (treePart.isTip()) {
			float parentBirth = (treePart.getParent()!=null ? treePart.getParent().getBirth() : 0);
			String branchLength = String.format("%.3f", treePart.getBirth()-parentBirth);
			int taxonIndex = tipsForNexus.indexOf(treePart)+1;
			String state = String.format("[&state=%d]",treePart.getDeme());
			returnValue+=(taxonIndex+state+":"+branchLength);
		}
		else if (treePart.getChildren().size()>0) {
			returnValue+="(";
			if (treePart.getChildren().size()!=2) {
				System.err.println("here\n");
			}
			returnValue+=newick(treePart.getChildren().get(0));
			for (int i = 1; i < treePart.getChildren().size(); i++){
				returnValue+=",";
				returnValue+=newick(treePart.getChildren().get(i));	
			}
			returnValue+=")";
			float parentBirth = (treePart.getParent()!=null ? treePart.getParent().getBirth() : 0);
			String branchLength = String.format("%.3f", treePart.getBirth()-parentBirth);
			String state = String.format("[&state=%d]",treePart.getDeme());
			returnValue+=(state+":"+branchLength);
		}

		return returnValue;
	}
	
	public static void printNexus(boolean printInternalNodes) {
		// TODO: convert tips to binary tree...
		try {
	
			tipsForNexus.clear();			
			File treeFile;
			if (!printInternalNodes) 
				treeFile = new File("out.tree");
			else
				treeFile = new File("outfull.tree");
			treeFile.delete();
			treeFile.createNewFile();
			PrintStream treeStream = new PrintStream(treeFile);
			treeStream.printf("#NEXUS\n\n");
			treeStream.printf("Begin taxa;\n");
			int ntaxa=0;
			for (Virus v : postOrderNodes(root)) {				
				if (v.isTip() || printInternalNodes) {
					tipsForNexus.add(v);
					ntaxa+=1;
				}
			}
			
			File regionsFile;
			if (!printInternalNodes) 
				regionsFile = new File("out.regions");
			else
				regionsFile = new File("outfull.regions");
			regionsFile.delete();
			regionsFile.createNewFile();
			PrintStream regionsStream = new PrintStream(regionsFile);			
			for (Virus v : tipsForNexus) {				
				regionsStream.printf("Simulated Influenza A Virus (A/SimTree/%.3f) %s\n", 1999+(v.getBirth()), v);
				regionsStream.printf("%d\n", v.getDeme());
			}
			regionsStream.close();
			
			treeStream.printf("\tDimensions ntax=%d;\n",ntaxa);
			treeStream.printf("\tTaxlabels\n");
					
			for (Virus v : tipsForNexus) {				
				treeStream.printf("\t\t'Simulated Influenza A Virus (A/SimTree/%.3f) %s'\n", 1999+(v.getBirth()), v);
			}
			treeStream.printf("\t\t;\n");		
			treeStream.printf("End;\n\n");
			
			treeStream.printf("Begin trees;\n");
			treeStream.printf("\tTranslate\n");
			for (int i = 0; i < tipsForNexus.size(); i++) {
				Virus v = tipsForNexus.get(i);
				treeStream.printf("\t\t%d 'Simulated Influenza A Virus (A/SimTree/%.3f) %s'", i+1,1999+(v.getBirth()), tipsForNexus.get(i));
				if ((i+1)!=tipsForNexus.size()) {
					treeStream.printf(",");
				}
				treeStream.printf("\n");
			}
			treeStream.printf("\t\t;\t\t\n");
			treeStream.printf("tree STATE_0 [&lnP=-0.0,posterior=-0.0] = [&R] "+newick());
			treeStream.printf("End;");
			treeStream.close();
		} catch(IOException ex) {
			System.out.println("Could not write to file"); 
			System.exit(0);
		}
		
	}
	
	
	
	private static void convertToBinaryTree(Virus v) {
		while (v.getChildren().size()==1) {
			v.collapse(v.getChildren().get(0));
		}
		if (v.getChildren().size()>2) {
			Virus newChildLikeParent = new Virus(v);			
			newChildLikeParent.setParent(v);			
			newChildLikeParent.fixParentOfChildren();
			newChildLikeParent.removeFirstChild();
			v.removeAllButFirstChild();
			v.addChild(newChildLikeParent);	
		}
		for (Virus child : v.getChildren()) {
			convertToBinaryTree(child);
		}		
	}
	
	public static void collapseStrongForward() {
		convertToBinaryTree(root);	
	}
	
	public static double[][][] getTransitions() {
		double[][][] returnValue = new double[Parameters.initialNs.length][Parameters.initialNs.length][12];	
		Virus root = findRoot(tips.get(0));
		getTransitions(root, returnValue);
		return returnValue;
	}
	
	private static Virus findRoot(Virus virus) {
		if (virus.getParent()!=null)
			return findRoot(virus.getParent());
		else 
			return virus;
	}
	
	private static void getTransitions(Virus node,
			double[][][] returnValue) {  // Decides month as half way through parent to child transition if less than year difference
		if (node.getParent()!=null) {
			if (node.getParent().getDeme()!=node.getDeme() && node.getParent().getBirth()>0 /*&& timeDiffParentChild<2.0/12.0*/) {
				int from = node.getParent().getDeme();
				int to = node.getDeme();
				int month = (int) ((((node.getBirth()+node.getParent().getBirth())/2.0)%1.0)*12.0);
				//int month = (int) ((node.getParent().getBirth()%1.0)*12.0);
				returnValue[from][to][month]+=1;
			}
		}
		for (Virus child : node.getChildren()) {
			getTransitions(child, returnValue);
		}
	}
	
	public static double[][] getNumberExtantLineages() {
		double[][] returnValue = new double[Parameters.initialNs.length][12];	
		Virus root = findRoot(tips.get(0));
		getNumberExtantLineages(root, returnValue);
		return returnValue;
	}
	
	private static void getNumberExtantLineages(Virus node,
			double[][] returnValue) {
		if (node.getParent()!=null) { // Only counts lineages with parent in the same location as child
			if (node.getParent().getDeme()==node.getDeme() && node.getParent().getBirth()>0) {
				double currentTime = node.getBirth();
				double parentTime = node.getParent().getBirth();
				int deme = node.getDeme();
				while (currentTime>=parentTime) {
					int month = (int) ((currentTime%1.0)*12.0);				
					returnValue[deme][month]+=1;
					currentTime-=(1.0/12.0);
				}
			}
			// TODO: add count lineages with different parent child location
		}
		for (Virus child : node.getChildren()) {
			getNumberExtantLineages(child, returnValue);
		}
	}
	

}