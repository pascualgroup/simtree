import java.text.DecimalFormat;



public class FxForNM implements Comparable<FxForNM> {
	double[] xs;
	double[] xmin;
	double[] xmax;
	double fmaxrange = 500;
	double penalty_slope = 10;
	double tol = 0.00001;
	double result; 
	boolean constrained=false;

	final public int oIsBETTER = 1;
	final public int oIsEQUAL = 0;
	final public int oIsWORSE = -1;
	private double constrained_result;

	private double meanDiv = 999;
	private double maxDiv =999;
	private double minDiv =999;
	private double numStrains =999;
	private double maxGap =999;
	private double inc;    

	DecimalFormat df = new DecimalFormat("#.##");

	String epitopeConfig="";

	
	
	public FxForNM(double[] xs_,double[] xmin_, double[] xmax_) {
		xs=xs_.clone();
		xmin=xmin_.clone();
		xmax=xmax_.clone();
		calcFx();
		constrained_result = result + pitFunction();		
		constrained=true;
	}

	public FxForNM(double[] xs_) {
		xs=xs_.clone();
		calcFx();
		constrained_result=result;
		constrained=false;
	}		

	private void calcFx() {		
		result =(xs[0]*xs[1]/xs[2]-xs[0]+xs[2]*xs[1]+xs[4]*xs[2]+xs[6]/xs[5]+xs[5]/(Random.nextDouble()*xs[7]));
		System.err.println(result);
		if (result<1.7) {
			System.out.println("breakhere!");
		}
	}

	public void printFx() {
		System.out.println("f(beta="+df.format(xs[0])+" nu="+df.format(xs[1])+" slope="+df.format(xs[2])+" muP="+df.format(xs[3])+" iPrR="+df.format(xs[4])+")="+df.format(result)+"\n\tepitope_config="+epitopeConfig+" min:"+df.format(xs[6])+" max: "+df.format(xs[7])+"\n\tmeanDiv="+df.format(meanDiv)+" maxDiv="+df.format(maxDiv)+" minDiv="+df.format(minDiv)+" numStrains="+numStrains+" maxGap="+df.format(maxGap)+" inc="+df.format(inc));
	}

	public String stringFx() {
		return "f(beta="+df.format(xs[0])+" nu="+df.format(xs[1])+" slope="+df.format(xs[2])+" muP="+df.format(xs[3])+" iPrR="+df.format(xs[4])+")="+df.format(result)+"\n\tepitope_config="+epitopeConfig+" min:"+df.format(xs[6])+" max: "+df.format(xs[7])+"\n\tmeanDiv="+df.format(meanDiv)+" maxDiv="+df.format(maxDiv)+" minDiv="+df.format(minDiv)+" numStrains="+numStrains+" maxGap="+df.format(maxGap)+" inc="+df.format(inc)+"\n";
	}

//	private void calcFx() {
//		Simulation sim = new Simulation();	
//
//		epitopeConfig="";
//		for (int i=0;i<((int) Math.round(xs[5])-1);i++) {
//			epitopeConfig=epitopeConfig+Random.nextInt((int) Math.round(xs[6]),(int) Math.round(xs[7]))+",";
//		}
//		epitopeConfig=epitopeConfig+Random.nextInt((int) Math.round(xs[6]),(int) Math.round(xs[7]));	
//		Parameters.applyArgsAndInit(new String[]{"beta="+xs[0],"nu="+xs[1],"slope="+xs[2]," myPhenotype=1E"+xs[3]," initialPrR="+xs[4],"nLoci="+(int)Math.round(xs[5]),"nStatesPerLoci="+epitopeConfig});
//		Parameters.printParams();
//
//
//		sim.reset();
//
//		sim.run();
//
//		meanDiv = sim.getMeanDiversity();
//		maxDiv= sim.getMaxDiversity();
//		minDiv = sim.getMinDiversity();
//		numStrains = sim.getNumSignificantStrains();
//		maxGap = sim.getMaxGapSize();
//		inc = sim.getYearlyIncidancePercent();
//		result=0.5*Math.abs(meanDiv-2.5)+4*Math.abs(maxDiv-7)+0.3*Math.abs(minDiv-1)+Math.abs(numStrains-15)+10*Math.abs(maxGap-2.2)+2*Math.abs(inc-9.5);
//
//	}

	private double pitFunctionSingle(double xs, double xmin, double xmax) {
		double penaltySlope = 100;
		double result = 0;
		if (xs>xmax) 
			result+=penaltySlope*Math.abs(xs-xmax);
		if (xs<xmin) 
			result+=penaltySlope*Math.abs(xs-xmin);
		result+=fmaxrange/(1+Math.exp((xs-xmin)/tol*10))-fmaxrange/(1+Math.exp((xs-xmax)/tol*10))+fmaxrange;
		return result;
	}
	private double pitFunction() {
		double output = 0;
		for (int i=0; i<xs.length;i++) {
			output=output+pitFunctionSingle(xs[i],xmin[i],xmax[i]);
		}
		return output;
	}

	public double fx() {			
		return result;		
	}

	public double constrained_fx() {

		return constrained_result;		
	}

	public double[] getXs() {
		return xs;
	}

	@Override
	public int compareTo(FxForNM o) {
		if (constrained) {
			if (constrained_result<o.constrained_fx()) return oIsWORSE;
			else if (constrained_result>o.constrained_fx()) return oIsBETTER;
			else return oIsEQUAL;
		}
		else {
			if (result<o.fx()) return oIsWORSE;
			else if (result>o.fx()) return oIsBETTER;
			else return oIsEQUAL;
		}
	}

	public FxForNM neighbour() {
		double[] newxs = xs.clone();;

		int numMutationSites = Random.nextInt(0,7);
		for (int i=0;i<numMutationSites;i++) {
			int site_index = Random.nextInt(0,7);
			newxs[site_index]=mutateIndividualX(site_index);
		}

		return new FxForNM(newxs,xmin,xmax);
	}

	private double mutateIndividualX(int site_index) {
		switch (site_index) {
		case 0: case 1 : case 2 : case 3 : case 4 : 
			double distToEdge = Math.min(Math.abs(xs[site_index]-xmin[site_index]),Math.abs(xs[site_index]-xmax[site_index])); 
			return xs[site_index]+Random.nextDouble(xs[site_index]-distToEdge, xs[site_index]+distToEdge);				
		case 5: case 6: case 7 : 
			return Random.nextDouble(xmin[site_index],xmax[site_index]);		

		}
		return 0;
	}

	


}