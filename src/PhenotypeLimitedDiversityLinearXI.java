/* Antigenic phenotype present in individual Viruses and within Hosts as immune history */
/* Should be able to calculate distance and cross-immunity between two phenotypes */
/* Moving up to multiple dimensions is non-trivial and requires thought on the implementation */
/* Multiple Viruses can reference a single Phenotype object */




public class PhenotypeLimitedDiversityLinearXI  implements Phenotype {

	// DANIEL 
	// parameters specific to PhenotypeLimitedDiversityLinearXI

	//enum InitialImmuneMemoryType {Random, InitialStrain };
	
	//static InitialImmuneMemoryType initialMemory = InitialImmuneMemoryType.Random;
	
	@Setting static boolean defaultNoXImmunity = false;
	@Setting static double slope = 0.13;												
	@Setting static int nLoci = 4;
	@Setting static int[] nStatesPerLoci =  {5,4,3,2};// {2,3,4,5,6};
	static long initialLimitedDiversityPhenotype = 0;
	public static enum PhenotypeMutationOptionType {Flipping, SingleTrait, SlowFast, NoMutation, SingleTraitEqualPropbablity};
	@Setting public static PhenotypeMutationOptionType PhenotypeMutationOption = PhenotypeMutationOptionType.SingleTrait;
		
	private static int nEpitopes = 0;  // in init()	
	private static int[] multiplyForIndex = null; // {1,2,6,24,120}; // in init()
	private static int[] sumForIndex = null; // {0,2,5,9,14,20}; // in init()

	// fields		
	private long traits = 0; 
	private long index;
	private static boolean initialized = false;
	private static long numPossibleStrains = 0;
	public static enum InitialMemoryType {Fixed, Random, Partial};
	@Setting public static InitialMemoryType initialMemory = InitialMemoryType.Random;
	static long partialLimitedDiversityPhenotype = 0;
	
	// phenotype output
	static final boolean outputBinary = false;

	public static void init() {		
		
		//initialLimitedDiversityPhenotype = 0;
		multiplyForIndex = new int[nLoci];
		sumForIndex = new int[nLoci];
		
		numPossibleStrains =1;
		for (int i=0;i<nStatesPerLoci.length;i++) {
			numPossibleStrains*=nStatesPerLoci[i];
		}	

		nEpitopes=0;
		for (int i=0;i<nStatesPerLoci.length;i++) {
			nEpitopes+=nStatesPerLoci[i];
		}	

		sumForIndex[0]=0;
		for (int i=1;i<nLoci;i++) {
			sumForIndex[i]=sumForIndex[i-1]+nStatesPerLoci[i-1];
		}

		multiplyForIndex[0]=1;
		for (int i=1;i<nLoci;i++) {
			multiplyForIndex[i]=multiplyForIndex[i-1]*nStatesPerLoci[i-1];
		}

		for (int i=0;i<nLoci;i++) {
			initialLimitedDiversityPhenotype=BitManip.set(initialLimitedDiversityPhenotype,sumForIndex[i]);
		}
		
		for (int i=0;i<nLoci;i+=2) {			
			partialLimitedDiversityPhenotype=BitManip.set(partialLimitedDiversityPhenotype,sumForIndex[i]);
		}
		for (int i=1;i<nLoci;i+=2) {			
			partialLimitedDiversityPhenotype=BitManip.set(partialLimitedDiversityPhenotype,sumForIndex[i]+1);
		}

		if (Parameters.param_index>=0) {

			double[] slope_range = new double[Parameters.gridpoints];
			for (int i=0; i<Parameters.gridpoints; i++) {
				slope_range[i]=0+((double) i)/((double)Parameters.gridpoints-1.0);
			}

			slope = slope_range[Parameters.param_index%slope_range.length];

		}
		if (Parameters.random_key_params) {
			slope = Random.nextDouble(0, 1);
		}
	}

	// constructors
	public PhenotypeLimitedDiversityLinearXI() {

		if (!initialized) {
			init();
		}
		traits = 0;

		for (int i=0;i<nLoci;i++) {
			traits=BitManip.set(traits,sumForIndex[i]);					
		}

		assert BitManip.cardinality(traits)==nLoci;
		
		
		index=(-1);

	}

	public PhenotypeLimitedDiversityLinearXI(long _traits) {			

		
		traits=_traits;	
		index = (-1);
		
		assert BitManip.cardinality(traits)==nLoci;
	}



	@Override
	public double riskOfInfection( ImmuneSystem immuneSystem) {

		assert BitManip.cardinality(traits)==nLoci;
				
		long history = ((ImmuneSystemEpitopeHistoryBased) immuneSystem).getHistory();

		assert  BitManip.cardinality(history)==0 ||  BitManip.cardinality(history)>=nLoci; 
		
		if (BitManip.cardinality(history) > 0) {

			long hasEncounteredBefore = history;

			hasEncounteredBefore&=traits;			
			long numAllelesEncounteredBefore = BitManip.cardinality(hasEncounteredBefore);
			long distance = nLoci - numAllelesEncounteredBefore; 
			// the distance is the number of virus epitopes not encountered before			

			assert distance<=nLoci;
			assert distance>=0;
			
			double risk;
			if (defaultNoXImmunity) {
				risk = 1 - slope*numAllelesEncounteredBefore;
			}
			else {
				risk = slope*distance; // more common in literature 					
			}

			
			assert  BitManip.cardinality(history)==0 ||  BitManip.cardinality(history)>=nLoci;
			
			if (risk>1) return 1;
			else if (risk<0) return 0;					
			else return risk;

		}
		else {
			return 1;
		}


	}

	// returns a random set of phenotype traits
	public static long randomTraits() {
		long returnValue = 0;

		for (int i=0; i<nLoci;i++) {			
			returnValue=BitManip.set(returnValue,sumForIndex[i]+Random.nextInt(0, nStatesPerLoci[i]-1)); 		
		}
		
		
		
		assert BitManip.cardinality(returnValue)==nLoci;

		return returnValue;
	}

	@Override
	public Phenotype mutate() {
		
		assert BitManip.cardinality(traits)==nLoci;

		switch (PhenotypeMutationOption) {
		case Flipping:
			return new PhenotypeLimitedDiversityLinearXI(randomTraits()); // returns a mutated version - this is basically a random version for this model
			
//		case SlowFast:
//			
//			int randLoci = Random.nextInt(0, nLoci-1);
//			
//			if ((randLoci<slowMutationRateStartingFromEpitope) || ((randLoci>=slowMutationRateStartingFromEpitope) && Random.nextBoolean(slowMutationRateRatio))) {
//				// randoms one of the traits....
//
//				BitSet newTraits = (BitSet) traits.clone();
//				newTraits.clear(sumForIndex[randLoci],sumForIndex[randLoci]+nStatesPerLoci[randLoci]);	
//				newTraits.set(sumForIndex[randLoci]+Random.nextInt(0, nStatesPerLoci[randLoci]-1));
//						
//				if (!newTraits.equals(traits)) {
//					return new PhenotypeLimitedDiversity(newTraits);
//				}
//				else {
//					return this;
//				}
//			}
//			else {
//				return this;
//			}
//
//			

		case SingleTrait:
			// randoms one of the traits....
			int randLoci = Random.nextInt(0, nLoci-1);
			long newTraits = traits;

			randLoci = Random.nextInt(0, nLoci-1);
			newTraits=BitManip.clear(newTraits,sumForIndex[randLoci],sumForIndex[randLoci]+nStatesPerLoci[randLoci]);	
			newTraits=BitManip.set(newTraits,sumForIndex[randLoci]+Random.nextInt(0, nStatesPerLoci[randLoci]-1));
			if (newTraits!=traits) {
				return new PhenotypeLimitedDiversityLinearXI(newTraits);
			}
			else {
				return this;
			}
				
			
		case SingleTraitEqualPropbablity:
			// randoms one of the traits....

			newTraits = traits;

			randLoci = Random.nextInt(0, nLoci-1);
		
			while (newTraits!=traits) {	
				newTraits=BitManip.clear(newTraits,sumForIndex[randLoci],sumForIndex[randLoci]+nStatesPerLoci[randLoci]);	
				newTraits=BitManip.set(newTraits,sumForIndex[randLoci]+Random.nextInt(0, nStatesPerLoci[randLoci]-1));		
			}			
			return new PhenotypeLimitedDiversityLinearXI(newTraits);
		
		case NoMutation: default: 
			return new PhenotypeLimitedDiversityLinearXI(traits);		
		}



	}

	@Override
	public String toString() {
		return toString(",");
	}

	// returns allele version at each loci "3,2,....,allele_version_at_loci_n"
	@Override
	public String toString(String seperator) {

		String returnValue = "";

		if (outputBinary) {
			for (int i=0; i<nEpitopes; i++) {
				if (BitManip.get(traits, i)) {
					returnValue = returnValue + "1";
				}
				else {
					returnValue = returnValue + "0";
				}
				if (i!=(nEpitopes-1)) {
					returnValue = returnValue +seperator;
				}			
			}			
		}
		else {
			for (int i=0; i<nLoci; i++) {
				for (int j=0; j<nStatesPerLoci[i]; j++) {
					if (BitManip.get(traits,sumForIndex[i]+j)) {
						returnValue = returnValue + j; 														
						if (i!=(nLoci-1)) {
							returnValue = returnValue + seperator;
						}
					}
				}
			}
		}

		return returnValue;
	}

//	public boolean increaseTraits() {
//
//		int[] intTraits = new int[nLoci];
//
//		long currentEpitope = 0;
//		long newTraits = 0;
//
//		for (int i=0; i<nLoci; i++) {
//			currentEpitope = BitManip.get(traits,sumForIndex[i], sumForIndex[i]+nStatesPerLoci[i]);				
//			intTraits[i]=BitManip.nextSetBit(currentEpitope);
//		}
//
//		intTraits[0]=intTraits[0]+1;
//		for (int i=0;i<(intTraits.length-1);i++) {
//			if (intTraits[i]>=PhenotypeLimitedDiversityLinearXI.nStatesPerLoci[i]) {
//				intTraits[i]=0;
//				intTraits[i+1]=intTraits[i+1]+1;
//			}
//		}
//		if (intTraits[PhenotypeLimitedDiversityLinearXI.nLoci-1]<PhenotypeLimitedDiversityLinearXI.nStatesPerLoci[PhenotypeLimitedDiversityLinearXI.nLoci-1]) {
//
//			for (int i=0; i<nLoci; i++) {
//				newTraits=BitManip.set(newTraits,sumForIndex[i]+intTraits[i]);
//			}					
//			traits = newTraits;
//			return true;
//		} 
//		else {		
//			traits = initialLimitedDiversityPhenotype;
//			return false;
//		}
//
//	}


	public long getIndex() {

		if (index==(-1)) {

			int currentEpitope = 0;

			long returnValue = 0;

			for (short i=0; i<nLoci; i++) {
				currentEpitope = BitManip.get(traits,sumForIndex[i], sumForIndex[i]+nStatesPerLoci[i]);				
				returnValue+=BitManip.nextSetBit(currentEpitope)*multiplyForIndex[i];
			}

			index = returnValue;

			return returnValue;
		}
		else {
			return index;
		}		
	}

	@Override
	public void add(ImmuneSystem immuneSystem) {	
		((ImmuneSystemEpitopeHistoryBased) immuneSystem).addEpitopes(traits);
	}

	public long getTraits() {
		return traits;
	}


	@Override
	public Number getPhenotypePart(int n) {
		if ((n >= nLoci) || (n<0)) {
			System.err.println("only parts 0.."+nLoci+" in phenotype..."); return 0; 
		}
		else {
			for (int j=0; j<nStatesPerLoci[n]; j++) {
				if (BitManip.get(traits,sumForIndex[n]+j)) {
					return j; 																			
				}
			}
		}
		System.err.println("error: part "+ n + " of phenotype not found!\n");
		return 0;
	}
	
	@Override
	public int numParts() {
		return nLoci;
	}

	public int getNumEpitopes() {
		// TODO Auto-generated method stub
		return nEpitopes;
	}
	
	public long getNumPossibleStrains() {
		return numPossibleStrains;
	}

	public boolean increaseTraits() {

		int[] intTraits = new int[nLoci];

		long currentEpitope = 0;
		long newTraits = 0;

		for (int i=0; i<nLoci; i++) {
			currentEpitope = BitManip.get(traits,sumForIndex[i], sumForIndex[i]+nStatesPerLoci[i]);				
			intTraits[i]=BitManip.nextSetBit(currentEpitope);
		}

		intTraits[0]=intTraits[0]+1;
		for (int i=0;i<(intTraits.length-1);i++) {
			if (intTraits[i]>=PhenotypeLimitedDiversityLinearXI.nStatesPerLoci[i]) {
				intTraits[i]=0;
				intTraits[i+1]=intTraits[i+1]+1;
			}
		}
		if (intTraits[PhenotypeLimitedDiversityLinearXI.nLoci-1]<PhenotypeLimitedDiversityLinearXI.nStatesPerLoci[PhenotypeLimitedDiversityLinearXI.nLoci-1]) {

			for (int i=0; i<nLoci; i++) {
				newTraits=BitManip.set(newTraits,sumForIndex[i]+intTraits[i]);
			}					
			traits = newTraits;
			return true;
		} 
		else {		
			traits = initialLimitedDiversityPhenotype;
			return false;
		}

	}

}

