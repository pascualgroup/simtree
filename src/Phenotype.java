/* Interface for Phenotype objects */


public interface Phenotype {
		
	// return mutated Phenotype object
	// returned Phenotype is a newly constructed copy of original
	Phenotype mutate();
	
	// this is used in output, should be a short string form
	// 2D: 0.5,0.6
	// sequence: "ATGCGCC"
	String toString(String seperator);
	
	String toString();
	
	// provides the risk of infection (from 0 to 1) of a virus with this phenotype 
	// when contacting a Host with a given immune system 
	double riskOfInfection(ImmuneSystem immuneSystem);	
	
	void add(ImmuneSystem immuneSystem);
	
	// get the nth parts of a phenotype....
	Number getPhenotypePart(int n);
	
	// get the number of parts in a phenotype....
	int numParts();

	// used in limited diversity phenotypes. get an index unique to a phenotype
	long getIndex();
	
	long getNumPossibleStrains();
	

}