


public interface ImmuneSystem {
	

	public void reset();
	
	public void add( Phenotype p);
	public double riskOfInfection( Phenotype p);
	
	String printState();
	
}
